var API = require('../../api/api.endpoint.js');
var PAY = require('../common/pay.js');
Page({
  data: {
    orderID: ''
  },

  onLoad: function (param) {
    this.data.orderID = param.orderID;
  },

  onShow: function () {
    this.getOrderInfo(this.data.orderID);
  },

  againPay: function () {
    PAY.payTap(this, this.data.orderID)
    // var url = '../payment/index?id=' + this.data.orderID;
    // wx.redirectTo({
    //   url: url
    // });
  },
  // 获取订单详情
  getOrderInfo: function (orderId) {
    // 支付
    var params = {};
    params.order = orderId; // 订单id

    wx.showToast({
      title: '请求中',
      icon: 'loading',
      duration: 10000
    });

    API.APIOrder.getOrderInfo(params).then(d => {
      wx.hideToast();
      var order = d.data.order;
      this.setData({
        consignee: order.consignee,
        total: order.total
      })
    })
  },

  //下拉刷新
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh()
  },
  // 进入订单详情页面
  pushOrderDetailTap: function () {
    // 交易成功  关闭当前页面，然后进入下级页面
    wx.redirectTo({
      url: '../orderDetail/index?orderID=' + this.data.orderID
    })
  },
})