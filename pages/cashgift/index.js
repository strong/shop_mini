var API = require('../../api/api.endpoint.js');
var app = getApp();
var common = require('../../utils/util.js');
Page({
    data: {
        cashgifts: [{
        }],
        show: true,     //是否展示空页面
        per_page : 100,
        total_price : 0,
        selectCashgift : '',    //选中的红包
    },

    //加载页面时给每个选中添加状态
    onLoad: function(options) {

        var data = this.data;
        var that = this;
        that.setData({
            total_price : options.total_price,
        })  
        if(options.id){
          data.id = options.id
        }
        if (options.goods_ids) {
          data.goods_ids = options.goods_ids
        }
        that.getCashgiftAvailable();
        this.setData(data);
    },
    //下拉刷新
    onPullDownRefresh: function(){
        this.getCashgiftAvailable();
         wx.stopPullDownRefresh()  
    },

    getCashgiftAvailable: function(){
      wx.showToast({
        title: '加载中',
        icon: 'loading',
        duration: 10000
      });
        var data = this.data;
        var that = this;
        var goods_ids = data.goods_ids
        var params = {};
        params.page = 1;
        params.per_page = data.per_page;
        params.total_price = data.total_price;
        if(goods_ids){
          params.goods_ids = goods_ids
        }
        API.APICashgiftAvailable.getCashgiftAvailable(params).then(d=>{
          
            if (d.data.cashgifts.length) {
              var id = that.data.id;
                d.data.cashgifts.map(function(item) {
                    item.effective = common.formatTime(item.effective);
                    item.expires = common.formatTime(item.expires);
                    if(id && id == item.id){
                      item.iconShow = true;
                    }
                })
                // console.log(d.data.cashgifts);
                // return ;
                //有数据时，默认选中第一个item
                // var index = that.data.index
                // if(index){
                //   d.data.cashgifts[index].iconShow = true;
                  
                // }
              
                that.setData({
                    cashgifts: d.data.cashgifts,
                    show : false,
                    is_red:0
                    // selectCashgift:d.data.cashgifts[0],
                });
                var pages = getCurrentPages();
                var prevPage = pages[pages.length-2];
                prevPage.setData({
                  cashgift: d.data.cashgifts[0],        
                });
                
            } else {
                that.setData({
                    show : true
                }); 
            }  
            wx.hideToast();
        });
    },


    //切换选择
    selectTap: function(e) {
        var data = this.data;
        data.cashgifts.map(function(item) {
            item.iconShow = false;
        });
        data.cashgifts[parseInt(e.currentTarget.id)].iconShow = true;
        data.selectCashgift = data.cashgifts[parseInt(e.currentTarget.id)];
        this.setData(data);

        var pages = getCurrentPages();
        var prevPage = pages[pages.length-2];
      
        prevPage.setData({
            cashgift : data.selectCashgift, 
            noUseCash: 'false',  
        })
        wx.navigateBack();
    },

    noUseCashgift : function(e){
        var data = this.data;
        var pages = getCurrentPages();
        var prevPage = pages[pages.length-2];
        prevPage.setData({
          selectCashgift:'',
            cashgift : '',  
            noUseCash : 'true',      
        })
        wx.navigateBack();
        
        this.setData(data);
    }

});
