// pages/evaluate/index.js

var API = require('../../api/api.endpoint.js');
var App = getApp();
var CONFIG = require('../../config/config.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    GRADE: {
      BAD: 1,     // 差评
      MIDDLE: 2,   // 中评
      GOOD: 3      // 好评
    },

    products: [],
    orderId: null,
    isAnonymous: false,
    reviews: [
    ],
    str:[],
    image:[],
    up_image:[],
    img_url: CONFIG.API_HOST
  },

  tapAnonymous: function() {
    this.setData({
      isAnonymous: !this.data.isAnonymous,
    })
  },

  submit: function() {
    var reviews = this.data.reviews;
    var image = this.data.image;
    // console.log(JSON.stringify(image))
    // return;
    var params = {};
    params.order = this.data.orderId;
    params.review = JSON.stringify(reviews);
    params.is_anonymous = this.data.isAnonymous ? 1 : 0;
    if(image){
      params.images = image
    }
    var that = this;
    
   
    setTimeout(function () {
      
      App.showLoading();
      

      API.APIOrder.reviewOrder(params).then(d => {
        
        App.showTips('评价成功');
        wx.redirectTo({
          url: '../evaluateResult/index',
        })
      }).catch(e => {
        App.showTips(e);
      })
    }, 1000)
   
    
    
  },

  tapFaceBad: function(e) {
    var data = this.data;
    var review = data.reviews[parseInt(e.currentTarget.id)];
    review.grade = data.GRADE.BAD;

    this.setData(data);
  },

  tapFaceMiddle: function (e) {
    var data = this.data;
    var review = data.reviews[parseInt(e.currentTarget.id)];
    review.grade = data.GRADE.MIDDLE;

    this.setData(data);
  },

  tapFaceGood: function (e) {
    var data = this.data;
    var review = data.reviews[parseInt(e.currentTarget.id)];
    review.grade = data.GRADE.GOOD;

    this.setData(data);
  },

  commentChange: function(e) {
    var data = this.data;
    var review = data.reviews[parseInt(e.currentTarget.id)];
    review.content = e.detail.value;

    this.setData(data);
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var data = this.data;
    //console.log(options.order)
    if (options.order) {
      var orderStr = options.order;
      var order = JSON.parse(orderStr); // 上页面传递的json串，这里需要转成json
      data.orderId = order.id;
      data.products = order.goods;

      // 评论内容
      data.products.map(good=> {
        data.reviews.push({
          goods: good.id,
          grade: data.GRADE.GOOD, // 默认好评
          content: '',            // 评价内容可以为空
        })
      })
    }

    this.setData(data);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },
  //删除图片
  delimg: function (e) {
    var index = e.currentTarget.dataset.index;
    var image = this.data.image;
    image.splice(index, 1)
    this.setData({
      image: image
    })
  },
  //处理用户选择的图片
  up: function (e) {
    var index = e.currentTarget.id;
    var that = this
    wx.chooseImage({
      count: 4,
      success: function (res) {
        var tempFilePaths = res.tempFilePaths
        
        for (var i = 0; i < tempFilePaths.length; i++) {
          var image = that.data.image
          var arr = image[index] == undefined ? [] : image[index];
          if (image[index] === undefined || image[index].length < 4) {
            
            wx.uploadFile({
              url: CONFIG.API_HOST + '//v2/ecapi.order.uploads', //仅为示例，非真实的接口地址
              filePath: tempFilePaths[i],
              name: 'image',
              header: {
                "Content-Type": "json"
              },
              dataType: 'json',
              success: function (res) {

                if (res.data != 0) {
                  arr.push(res.data)
                  image[index] = arr
                  that.setData({
                    image: image
                  })
                }
              }
            })
            
          } else {
            wx.showModal({
              title: '提示',
              showCancel: 'false',
              content: '您选择超过了四张图片，已为您选择你优先选择的图片'
            })
          }
        }
        console.log(arr)
        return ;
        
        
        
      }
    })
  }
})