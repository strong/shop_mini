// pages/redbonus/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    src:'https://wechat.y-med.com.cn/wb/index.php/index/index/hongbao',
    is_auth: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('l')
    var that = this;
   var data = {};
    data.act_id = options.act_id;
    var act_id = options.act_id
    
    if(options.is_auth){
      data.is_auth = options.is_auth
      if (!app.isLogin()) {
        wx.navigateTo({
          url: '../authorize/index'
        })
        return;
      }
    }
    if (options.act_id && options.act_id!=undefined) {
      wx.setStorageSync('act_id', options.act_id);
    }else{
      var act_id = wx.getStorageSync('act_id');
      this.setData({
        act_id: act_id
      })
    } 


    wx.request({
      url: 'https://api.xiaomaiyouxuan.com//v2/ecapi.bonus_active.get',
      method: "POST",
      data: { act_id: act_id },
      success: function (e) {
        var data = e.data
        if (data.code == 100) {
          that.setData({
            activedata:data.data
          })
        }
      }
    })
    
    this.setData(data)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
      var act_id = this.data.act_id;
      var is_auth = this.data.is_auth;
      if(act_id==undefined){
        act_id = wx.getStorageSync('act_id')
        
      }
     
      var u = wx.getStorageSync('u');
      var src = this.data.src;
         src += "?mobile="+u.mobile+"&act_id="+act_id+'&is_auth=0';
      this.setData({
        src: src
      })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var activedata = this.data.activedata;
    
    if(activedata){
      var share_title = activedata.share_title ? activedata.share_title : active_name;
      var share_path = activedata.share_path ? activedata.share_path : '';
      var share_img = activedata.share_img ? activedata.share_img : '';
    }
    return {
      title: share_title,
      path: share_path,
      imageUrl: share_img
    }
  }
})