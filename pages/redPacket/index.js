var API = require('../../api/api.endpoint.js');
var app = getApp();
Page({
    data: {
        packetData: {},
        isShow: false,
    },

    openNext: function () {
        this.setData({
            isShow: true,
        });
    },
    onLoad: function (param) {
        this.getRedPacket();
    },
    getRedPacket: function () {
      var params = {};
      params.type_name = '新用户专属礼包';
        API.APIPacket.getRedPacket(params).then(res => {
            console.log(res);
            this.data.packetData = res.data;
        })
    },
    getRedPack: function () {
        const packetData = this.data.packetData;
        const that = this;
        if (!app.isLogin()) {
            wx.navigateTo({
                url: '../authorize/index'
            })
            return;
        } else {
            wx.getStorage({
                key: "u",
                success: function (res) {
                      if(packetData.code==100){
                        const params = {mobile:res.data.username,bonus_sn:packetData.data.bonus_sn};
                        API.APIPacket.bindRedPacket(params).then(res =>{
                          console.log(res);
                          if(res.data.code===100){
                            
                            wx.setStorageSync('new_user_bonus', 1)
                            wx.showToast({
                              title: '领取成功',
                              duration: 2000,
                              success: function () {
                                setTimeout(function () {
                                  that.toIndex();
                                }, 2000) //延迟时间 
                              }
                            })  
                          }else{
                            wx.showModal({
                              title: '提示',
                              content: '您已经领取过该红包了',
                              showCancel:false,
                              success(res) {
                                if (res.confirm) {
                                  wx.navigateBack({     //返回上一页面或多级页面
                                    delta: 1
                                  }) 
                                }
                              }
                            })
                            
                            
                          }
                         
                      })
                      }else{
                        wx.showToast({
                            title:packetData.msg,
                            icon:'none',
                            duration: 2000,
                            
                        })
                      }
                     
                }
            })
        }
    },
    

    toIndex: function () {
       
        wx.navigateBack({
            delta: 1, // 回退前 delta(默认为1) 页面
        },)
    },
    
})
