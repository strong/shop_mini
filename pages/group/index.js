//index.js
//获取应用实例
var API = require('../../api/api.endpoint.js');
Page({
  data: {
  
    inputFocus: false,
    inputValue: "",
    title: '加载中...',
    loading: false,
		
 
  },
 
onLoad: function (options) {
	var that = this;
	var CONFIG = require('../../config/config.js');
    wx.request({
	
      url: CONFIG.API_HOST + "/v2/ecapi.group.list",
            method: 'POST',
            data: {
             },
             header: {
             'content-type': 'application/x-www-form-urlencoded'
      },
	
      success: function (res) {
        console.log(res.data)
        that.setData({
          goods: res.data.products
        })
    //     that.setData({ categories: res.data.data.categories }); 
    //     that.setData({ arr_ad1: res.data.data.arr_ad1}); 
    //     that.setData({ cat_detail: res.data.data.cat_detail }); 
		// that.setData({ currentItemId:options.id});
    //     that.setData({ all_cates: res.data.data.all_cates})
		// wx.setNavigationBarTitle({title:res.data.data.arr_ad1[0].ad_name+"频道"});
        wx.hideNavigationBarLoading();
      }
    })
 
    //  console.log(app.globalData.openid);
  wx.showNavigationBarLoading();
   
  },

tapMenuItem: function(e) {
 
    this.setData({  
      currentItemId:e.currentTarget.id   
    })  
    },
	
/*tapName: function(event) { 
 console.log(event) ;
  wx.pageScrollTo({
  scrollTop: event-800,
  duration: 300
})
  },*/
  

tapProductList: function(e) {
        var CategoryId = parseInt(e.currentTarget.id);
		var url = '../productList/index?cid='+CategoryId;
        wx.navigateTo({
            url: url
        });
   },
tapProduct: function(e) {

  var productId = parseInt(e.currentTarget.id);
  var act_id = parseInt(e.currentTarget.dataset.act_id);
  var url = '../group/detail?pid=' + productId+"&act_id="+act_id;

  wx.navigateTo({
      url: url
  });
},


  tapAd: function(e) {
    var url = String(e.currentTarget.id);
        wx.navigateTo({
          url: url
        });
    },
    bindInput: function(e) {
        this.setData({
            inputFocus: true,
            inputValue: e.detail.value
        });
    },

    tapCancel: function(e) {
        this.setData({
            inputFocus: false,
            inputValue: ""
        });
    },
    bindChange: function(e) {
        var url = "../searchResult/index?keyword=" + this.data.inputValue;
        wx.navigateTo({ url: url });
    },

})