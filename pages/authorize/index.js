var API = require('../../api/api.endpoint.js');
var CONFIG = require('../../config/config.js');
var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },

  // 微信登录
  onGotUserInfo: function (e) {
    
    var that = this;
    this.setData({
      userInfo: e.detail.userInfo
    });
    wx.setStorageSync('u', e.detail.userInfo);
    app.showLoading();
    wx.login({
      success: function (res) {   
        var VENDER_WXA = 5;
        var params = {
          avatar: e.detail.userInfo.avatarUrl,
          nickname: e.detail.userInfo.nickName
        };
        params.vendor = VENDER_WXA;
        params.js_code = res.code;
        if (getApp().globalData.sharerUid) { // 如果有推荐人id，需要传推荐人id参数
          params.invite_code = getApp().globalData.sharerUid;
        } 
        
        API.APIAuthSocial.authSocial(params).then(d => {
          
          var userInfo = d.data.user;
          var wxUserInfo = that.data.userInfo;
          userInfo.nickname = wxUserInfo.nickName
          var photo = {};
          photo.thumb = wxUserInfo.avatarUrl;
          photo.large = wxUserInfo.avatarUrl;
          userInfo.photo = photo;
          userInfo.gender = wxUserInfo.gender;
          wx.setStorageSync('u', userInfo);
          getApp().globalData.userInfo = userInfo;

          
          app.hideLoading();
          //console.log(d)
          var openid = d.data.openid;

          getApp().globalData.openid = openid;
          wx.setStorageSync('o', openid);

          wx.setStorageSync('t', d.data.token);
          getApp().globalData.token = d.data.token;
          wx.setStorageSync('session_key', d.data.session_key);
          //console.log(userInfo)
          

          var params = {};
          params.nickname = userInfo.nickname;
          params.gender = userInfo.gender;

          API.APIUser._update(params).then(d => {
            wx.navigateBack({
              delta: 1
            });
          });
          
        }).catch(e => {
          
          if(e == 40004){
            wx.navigateTo({
              url: '/pages/bindMobile/index?type=signup'
            });
          }
          app.hideLoading();
          app.showTips(e);
          
        })
        
      },
      fail: function (res) {
        app.hideLoading();
        app.showTips('微信登录失败');
      }
    });
  },

  goSignin: function () {
    wx.navigateTo({
      url: '../signin/index',
    })
  },

  goSignup: function () {
    wx.navigateTo({
      url: '../signup/index?type=signup',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var data = this.data;
    this.setData(data);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },
})