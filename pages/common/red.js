var API = require('../../api/api.endpoint.js');
var CONFIG = require('../../config/config.js');
var REDAPI = require('../../api/api.endpoint.js');
var app = getApp();
var checkRed = function(that){
  API.APIConfig.smallBonus().then(res => {
    //console.log()
    if (res.data.code == 100) {
      var bonus_name = res.data.data.config.bonus_name
      //检测后台配置红包是否开启
      if (res.data.data.config.status == 1) {
        getRedPacket(that,bonus_name);
        that.setData({
          bonus_name: bonus_name
        })

      }
    }
  })
}
var getRedPacket= function(that,bonus_name) {
  
  var params = {};
  params.type_name = bonus_name;
  REDAPI.APIPacket.getRedPacket(params).then(res => {
    
    if (res.data.code == 100) {
      
      let packetData = res.data.data;
      if (app.isLogin()) {
        var params = {};
        
        params.type_name = bonus_name;
        params.mobile = wx.getStorageSync('u')['mobile']
        
        API.APIPacket.isReceiveRedBonus(params).then(res => {

          if (res.data.code === 100) {
            var commmon_paket = getApp().globalData.commmon_paket
            
            that.setData({
              showView: !commmon_paket,
              packetData: packetData,
              commmon_paket: commmon_paket
            })
            getApp().globalData.showView = !commmon_paket;
          }
        })
      } else {
        var new_user_bonus = wx.getStorageSync('new_user_bonus2');
       
        if (!new_user_bonus) {
          that.setData({
            showView: true,
            commmon_paket: false
          })
          getApp().globalData.showView = true;
        }
      }
    } else {
      that.setData({
        showView: false
      })
      getApp().globalData.showView = false;
    }

  })
}
//关闭红包
var closeImg = function(that) {
  if (app.isLogin()) {
    var params = {};

    params.type_name = that.data.bonus_name;
    params.mobile = wx.getStorageSync('u')['mobile']

    API.APIPacket.isReceiveRedBonus(params).then(res => {
      var data = {
        showView: false,

      }
      if (res.data.code === 100) {
        app.globalData.commmon_paket = true
        data.commmon_paket = true
      }
      that.setData(data)
    })
  } else {
    var new_user_bonus = wx.getStorageSync('new_user_bonus2');
    var data = {
      showView: false,

    }
    if (!new_user_bonus) {
      data.commmon_paket = true
      app.globalData.commmon_paket = true
    }
    that.setData(data)
  }

}
//展示红包
var showView = function(that) {
  that.setData({
    showView: true,
    commmon_paket: false
  })
  app.globalData.showView = true
  app.globalData.commmon_paket = false
}

var getRed = function(that) {

  const packetData = that.data.packetData;
  if (!app.isLogin()) {
    wx.navigateTo({
      url: '../authorize/index'
    })
    return;
  } else {
    if (packetData && packetData.length > 0) {
      let bunus_sn = ''
      packetData.forEach(item => {
        bunus_sn = bunus_sn + ',' + item.bonus[0].bonus_sn
      });


      var params = {};
      params.bonus_sn = bunus_sn;
      params.mobile = wx.getStorageSync('u')['mobile']
      REDAPI.APIPacket.bindRedPacket(params).then(res => {
        console.log(res);

        if (res.data.code === 100) {
          wx.setStorageSync('new_user_bonus2', 1)
          wx.showToast({
            title: '领取成功',
            duration: 2000,
            success: function () {
              that.setData({
                showView: false
              })
              getApp().globalData.showView = false
              app.globalData.commmon_paket = false
            }
          })
        } else {
          wx.showModal({
            title: '提示',
            content: res.data.msg,
            showCancel: false,
            success(res) {
              that.setData({
                showView: false
              })
              getApp().globalData.showView = false
              app.globalData.commmon_paket = false
            }
          })


        }

      })
    } else {
      wx.showToast({
        title: '您已经领取',
        duration: 2000,
        success: function () {
          that.setData({
            showView: false
          })
          getApp().globalData.showView = false
          app.globalData.commmon_paket = false
        }
      })
    }
  }
}


module.exports = {
  checkRed: checkRed,
  closeImg: closeImg,
  showView: showView,
  getRed: getRed
};