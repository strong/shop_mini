var API = require('../../api/api.endpoint.js');
var CONFIG = require('../../config/config.js');
var REDAPI = require('../../api/api.endpoint.js');

// 微信支付
var payTap = function (that,id,isgo=0) {
  var params = {};
  params.order = id; // 订单id
  params.code = 'wxpay.wxa'; // 微信支付
  params.openid = getApp().globalData.openid;
  if (getApp().globalData.sharerUid) { // 如果有推荐人id，需要传推荐人id参数
    params.invite_code = getApp().globalData.sharerUid;
  }
  // params.invite_code = 104;
  wx.showToast({
    title: '请求中',
    icon: 'loading',
    duration: 10000
  });

  // 支付
  API.APIPayment.weixinPayment(params).then(d => {
    wx.hideToast();
    wx.requestPayment({
      timeStamp: d.data.wxpay.timestamp,
      nonceStr: d.data.wxpay.nonce_str,
      package: "prepay_id=" + d.data.wxpay.prepay_id,
      signType: 'MD5',
      paySign: d.data.wxpay.sign,
      success: function (res) {
        if (res.errMsg == 'requestPayment:ok') {
          // 交易成功  关闭当前页面，然后进入下级页面
          wx.redirectTo({
            url: '../paySuccess/index?orderID=' + id
          })
        } else { //支付失败
          wx.redirectTo({
            url: '../payState/index?orderID=' + id
          })
        }

      },
      fail: function () {
        
        if(isgo == 1){
          wx.redirectTo({
            url: '../payState/index?orderID=' + id
          })
        }
        
      },
      complete: function () {
        // complete
      }
    })
  })
}


module.exports = {
  payTap: payTap
};