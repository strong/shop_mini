var CONFIG = require('../../config/config.js');
var app = getApp();
var common = require('../../utils/util.js');
var RED = require('../common/red.js');
//获取应用实例
var API = require('../../api/api.endpoint.js');
Page({
  data: {

    inputFocus: false,
    inputValue: "",
    title: '加载中...',
    loading: false,
    currentItemId: "1",
    showView: false, //getApp().globalData.showView
    api_host: CONFIG.API_HOST,
    commmon_paket: false,
  },

  onLoad: function() {
    //this.reload();
    //this.tapWxaSignin();
    var that = this;
    var CONFIG = require('../../config/config.js');
    //console.log(CONFIG.API_HOST + "/wx17bf0cd305c23147/catalog.php")
    wx.request({

      url: CONFIG.API_HOST + "/wx2/catalog.php",
      method: 'GET',
      data: {
        //name: 'qinziheng',
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: function(res) {
        // console.log(res.data)
        that.setData({
          arr_ad1: res.data.data.arr_ad1
        });
        that.setData({
          categories: res.data.data.categories
        });
        that.setData({
          currentItemId: res.data.data.arr_ad1[0].link_qt
        });
        wx.hideNavigationBarLoading();
      }
    })

    //  console.log(app.globalData.openid);
    wx.showNavigationBarLoading();
    RED.checkRed(this);
  },

  tapMenuItem: function(e) {

    this.setData({
      currentItemId: e.currentTarget.id
    })
  },

  /*tapName: function(event) { 
   console.log(event) ;
    wx.pageScrollTo({
    scrollTop: event-800,
    duration: 300
  })
    },*/

  tapProductList: function(e) {
    var CategoryId = parseInt(e.currentTarget.id);
    var url = '../productList/index?cid=' + CategoryId;
    wx.navigateTo({
      url: url
    });
  },


  tapAd: function(e) {
    var url = String(e.currentTarget.id);
    wx.navigateTo({
      url: url
    });
  },
  bindInput: function(e) {
    this.setData({
      inputFocus: true,
      inputValue: e.detail.value
    });
  },

  tapCancel: function(e) {
    this.setData({
      inputFocus: false,
      inputValue: ""
    });
  },
  bindChange: function(e) {
    var url = "../searchResult/index?keyword=" + this.data.inputValue;
    wx.navigateTo({
      url: url
    });
  },
  //关闭遮盖
  closeImg: function(e) {
    RED.closeImg(this)
  },
  //显示大的红包
  showView: function() {
    RED.showView(this)
  },
  //领取红包
  getRed: function(e) {
    RED.getRed(this)
  },

})