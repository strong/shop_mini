var API = require('../../api/api.endpoint.js');

Page({
    data: {
        imageName : 'circle',
        regions: [], // 地区
        provinceRegions : [], // 省
        cityRegions : [], // 市
        districtRegions : [], // 区
        provinceIndex: 0,
        cityIndex: 0,
        districtIndex: 0,
        provinRegionName:'',
        cityRegionName:'',
        districtRegionName:'',
        isModify:false,
        currentDistrictRegionConsignee: {},
        consignee:{},
        addressid:0,
    },

    //下拉刷新
    onPullDownRefresh: function () {
      wx.stopPullDownRefresh()
    },
    
    onLoad: function(param) {
        this.data.consignee.zip_code = '000000';
      this.loadRegionsCache(param);
    const that  = this;
    // wx.chooseAddress({
    //     success (res) {
    //       var params = {};
    //     //   var data = this.data;
    //     //   params.consignee = data.consignee.id;
    //       params.name = res.userName;
    //       params.mobile = res.telNumber;
    //       params.address = res.detailInfo;
    //       params.tel = res.telNumber;
    //       params.zip_code = res.postalCode;
    //       res.provinceName=res.provinceName.replace('市','');
    //       let str = res.provinceName+','+res.cityName+','+res.countyName
    //       that.setAddress(str);
    //     that.setData({
    //         consignee:params,
    //     })
    //     }
    //   })
    },

    // 获取收货地址
    loadRegionsCache: function (param) {
        var that = this;
        // 获取地区列表缓存数据
        wx.getStorage({
            key: 'regions',
            success: function(res) {
                if ( typeof(res.data) == "string" ) {
                    that.regions = JSON.parse(res.data);
                } else {
                    that.regions = res.data;
                }
                if (that.regions.length == 0) {
                  // 没有缓存，请求接口
                    that.reloadRegionsModel(1, param);
                } else {
                  // 区域数据初始化
                    that.reloadRegionListData();
                }

                // 设置默认数据
                that.reloadViewData(param);
            },
            fail: function() {
              // 没有缓存，请求接口
                that.reloadRegionsModel(2,param);
                // 设置默认数据
            }
        })
    },
    setAddress: function (param){
        const value = {regions:param};
        API.APIConsignee.getAddressMessage(value).then(res =>{
            this.setData({
                provinceIndex:res.data.data[0].id,
                cityIndex:res.data.data[1].id,
                districtIndex:res.data.data[2].id,
                provinRegionName:res.data.data[0].name,
                cityRegionName:res.data.data[1].name,
                districtRegionName:res.data.data[2].name,
                addressid:res.data.data[2].id,
            })
        })

    },
    // 设置默认数据
    reloadViewData: function (param) {
        var that = this;

        if ( param.item ) // 如果有值就是编辑，item为列表传过来的地址
        {
            that.data.isModify = true;
            param.item = JSON.parse(param.item)

            wx.setNavigationBarTitle({
              title: '编辑收货地址',
            })

            // 如果为修改，那么就初始化默认数据
            var provinRegionIndex = 0;
            var cityRegionIndex = 1;
            var districtRegionIndex = 2;

            if ( param.item.regions.length > 3 )
            {
                provinRegionIndex = 1;
                cityRegionIndex = 2;
                districtRegionIndex = 3;
            }

            // 默认地址
            var defaultAddressImageName = param.item.is_default ? 'success' : 'circle';

            if(that.regions.length != 0){
                var currentCountryConsignee = that.regions[0];
                var currentProvinceRegions = currentCountryConsignee.regions;

                var  defaultprovinRegionName =  param.item.regions[provinRegionIndex].name;
                currentProvinceRegions.forEach(function(item,index){
                    if (item.name == defaultprovinRegionName) {
                        that.data.provinceIndex = index;
//                        that.setData(that.data);
                    }
                });

                var currentProvinceConsignee = currentProvinceRegions[that.data.provinceIndex];
                var currentCityRegions = currentProvinceConsignee.regions;

                var defaultcityName = param.item.regions[cityRegionIndex].name;
                currentCityRegions.forEach(function(item,index){
                    if (item.name == defaultcityName) {
                        that.data.cityIndex = index;
//                        that.setData(that.data);
                    }
                });

                var currentCityRegionConsignee = currentCityRegions[that.data.cityIndex];
                var currentDistrictRegions = currentCityRegionConsignee.regions;

                var defaultdistrictName = param.item.regions[districtRegionIndex].name;
                currentDistrictRegions.forEach(function(item,index){
                    if (item.name == defaultdistrictName) {
                        that.data.districtIndex = index;
//                       that.setData(that.data);
                    }
                });

                that.setData({
                    consignee:param.item,
                    currentDistrictRegionConsignee : param.item,
                    provinRegionName : param.item.regions[provinRegionIndex].name,
                    cityRegionName : param.item.regions[cityRegionIndex].name,
                    districtRegionName : param.item.regions[districtRegionIndex].name,
                    imageName: defaultAddressImageName,
                    isModify: true,
                })

                that.reloadRegionListData();
            }  
        }
        else
        {
            that.setData({
                isModify: false,
            })
            // 添加地址
            wx.setNavigationBarTitle({
              title: '添加收货地址',
            })
        }
    },

    // 获取区域
    reloadRegionsModel:function(type,param){
        var that = this;
        //调用应用实例的方法获取全局数据
        API.APIRegion.getRegionList().then(d=> {
            // 判断当前数组的长度，如果为空那么就显示地址为空
            if ( d.data.regions.length ) {
                that.regions = d.data.regions;
                wx.setStorage({
                  key:"regions",
                  data:d.data.regions
                })
                that.reloadRegionListData();
                if (type == 2){
                  that.reloadViewData(param);
                }

            }
            else
            {
                //更新数据
                that.setData({
                });
            }
        })
    },

    // 区域数据初始化
    reloadRegionListData: function() {
        var that = this;
        var regionStr = JSON.stringify(that.regions);
        var regions = JSON.parse(regionStr);
        if(regions.length != 0){
            var currentCountryConsignee = regions[0];
            var currentProvinceRegions = currentCountryConsignee.regions;

            var currentProvinceConsignee = currentProvinceRegions[that.data.provinceIndex];
            var currentCityRegions = currentProvinceConsignee.regions;

            var currentCityRegionConsignee = currentCityRegions[that.data.cityIndex];
            var currentDistrictRegions = currentCityRegionConsignee.regions;

            //更新数据
            currentProvinceRegions.forEach(function(item,index){
                item.regions = null;
            });

            that.data.provinceRegions = currentProvinceRegions;
            that.setData(that.data);

            var newcurrentCityRegions = [];
            currentCityRegions.forEach(function(item,index){
                item.regions = null;
            });

            that.data.cityRegions = currentCityRegions;
            that.setData(that.data);

            var newcurrentDistrictRegions = [];
            currentDistrictRegions.forEach(function(item,index){
                item.regions = null;
            });

            that.data.districtRegions = currentDistrictRegions;
            that.setData(that.data);
        }
    },

    // 更改view上选中的区域数据
    reloadRegionData: function() {
        var that = this;
        if (that.regions.length != 0) {
            var currentCountryConsignee = that.regions[0];
            var currentProvinceRegions = currentCountryConsignee.regions;

            var currentProvinceConsignee = currentProvinceRegions[that.data.provinceIndex];
            var currentCityRegions = currentProvinceConsignee.regions;

            var currentCityRegionConsignee = currentCityRegions[that.data.cityIndex];
            var currentDistrictRegions = currentCityRegionConsignee.regions;

            var defaultcityName = currentCityRegionConsignee.name;
          // return ;

              that.data.currentDistrictRegionConsignee = currentDistrictRegions[that.data.districtIndex];

            //更新数据
            that.setData({
                // 省份数据
                provinRegionName: currentProvinceConsignee.name,

                // 城市数据
                cityRegionName: currentCityRegionConsignee.name,

                // 区域数据
                districtRegionName: that.data.currentDistrictRegionConsignee.name,

                // 更新当前区域
                currentDistrictRegionConsignee : that.data.currentDistrictRegionConsignee,
            });

        } else {
            
        }
    },

    //设置为默认地址
    defaultTap: function() {
      if (this.data.isModify) {
        this.setupDefault(this.data.consignee.id);
      } else {
        // 新建地址时
        var currentConsignee = this.data.consignee;
        currentConsignee.is_default = currentConsignee.is_default == undefined ? true : !currentConsignee.is_default;
        this.setData({
          imageName: currentConsignee.is_default ? 'success' : 'circle',
          consignee: currentConsignee
        })
      }
    },

    setupDefault: function (consigneeId) {
      // 逻辑：没有取消默认地址，设置一个新的默认地址时，旧的默认地址自动取消
      if (this.data.isModify && this.data.consignee.is_default) {
        return; // 如果已经是默认地址了，就什么都不做
      }

      var params = {};
      params.consignee = consigneeId;
      getApp().showLoading();
      API.APIConsignee.defaultConsignee(params).then(d => {
        getApp().hideLoading();

        var currentConsignee = this.data.consignee;
        currentConsignee.is_default = true;
        this.setData({
          imageName: 'success',
          consignee: currentConsignee
        })

        wx.showToast("设置默认地址成功");
      }).catch(e=>{
        getApp().hideLoading();
        getApp().showTips(e)
      })
    },

    // 名称变化
    bindNameInput: function(e) {
        this.data.consignee.name = e.detail.value;
    },

    // 手机号变化
    bindMobileInput: function(e) {
        this.data.consignee.mobile = e.detail.value;
    },
    
    // 详细地址变化
    bindAddressInput: function(e) {
        this.data.consignee.address = e.detail.value;
    },

    bindZipCodeInput: function(e) {
        this.data.consignee.zip_code = e.detail.value;
    },

    //picker值改变时
    provincePickerChange: function(e) {
        this.data.provinceIndex = e.detail.value;
        this.data.cityIndex = 0;
        this.data.districtIndex = 0;
        this.reloadRegionData();
        this.reloadRegionListData();
    },

    cityPickerChange: function(e) {
        var that = this;
        this.data.cityIndex = e.detail.value;
        this.data.districtIndex = 0;
        this.reloadRegionData();
        this.reloadRegionListData();
    },

    districtPickerChange: function(e) {
        this.data.districtIndex = e.detail.value;
        this.reloadRegionData();
        this.reloadRegionListData();
    },

    bindConsignee: function(e) {
        var params = {};
        var data = this.data;
        params.consignee = data.consignee.id;
        params.name = data.consignee.name;
        params.mobile = data.consignee.mobile;
        params.address = data.consignee.address;
        params.tel = data.consignee.tel;
        params.zip_code = data.consignee.zip_code;

        var region = data.currentDistrictRegionConsignee.id;
        if (data.currentDistrictRegionConsignee.regions && data.currentDistrictRegionConsignee.regions.length > 0) {
          region = data.currentDistrictRegionConsignee.regions[data.currentDistrictRegionConsignee.regions.length - 1].id;
        }
        params.region = region;

        var tips = '';
        if ( params.name )
        {
            if ( !params.name.length )
            {
                getApp().showTips('请填写收件人姓名');
                return;
            }

        }
        else
        {
           
            getApp().showTips('请填写收件人姓名');
            return;
        }

        if ( !(params.mobile && params.mobile.length) )
        {
           
            getApp().showTips('请填写手机号码');
            return;
        }

        if ( !params.region )
        {
            if(this.data.addressid==0){
                getApp().showTips('请选择所在地区');
                return;
            }else{
                params.region = this.data.addressid
            }
            
            
        }


        if ( !(params.address && params.address.length) )
        {
            
            getApp().showTips('请填写详细地址');
            return;
        }
        
        wx.showToast({
            title: '加载中',
            icon: 'loading',
            duration: 10000
        });
        if ( this.data.isModify )
        {
            this.updateConsignee(params);
        }
        else
        {
            this.addConsignee(params);
        }

    },

    back: function(){
        wx.navigateBack({
            delta: 1, // 回退前 delta(默认为1) 页面
        })
    },

    // 修改收件人
    updateConsignee: function(params){
        API.APIConsignee.updateConsignee(params).then(d => {
            var data = this.data;
            this.setData(data);
            wx.hideToast();
            wx.navigateBack({
              delta: 1, // 回退前 delta(默认为1) 页面
            })
        })
    },

    // 添加收件人
    addConsignee: function (params){
        API.APIConsignee.addConsignee(params).then(d => {
          wx.hideToast();
            
            if (this.data.consignee.is_default) {
              this.setupDefault(d.data.consignee.id);
            }
            wx.navigateBack({
              delta: 1, // 回退前 delta(默认为1) 页面
            })
        })
    },
})