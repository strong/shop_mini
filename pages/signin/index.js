var API = require('../../api/api.endpoint.js');
var CONFIG = require('../../config/config.js');
const XXTEA = require('../../utils/xxtea.js');
const Util = require('../../utils/util.js');
var app = getApp();

Page({
    data: {
      loginBtnEnable: false,
      inputUsername: '',
      inputPassword: '',
    },

    //加载页面时给每个选中添加状态
    onLoad: function () {
      var data = this.data;
      this.setData(data);
    },

    clickForgot: function() {
      wx.navigateTo({
        url: '../signup/index?type=forgot',
      })
    },

    // username input
    bindUsernameInput: function(e) {
      this.setData({
        inputUsername: e.detail.value,
        loginBtnEnable: (e.detail.value.length > 0 && this.data.inputPassword.length > 0),
      });
    },

    // password input
    bindPasswordInput: function (e) {
      this.setData({
        inputPassword: e.detail.value,
        loginBtnEnable: (this.data.inputUsername.length > 0 && e.detail.value.length > 0),
      });
    },

    //下拉刷新
    onPullDownRefresh: function(){
       wx.stopPullDownRefresh()  
    },
    
    /*
    onGotUserInfo: function (e) {
        var that = this;
        this.setData({
            userInfo: e.detail.userInfo
        });
         wx.login({
                success: function(res) {
                    var VENDER_WXA = 5;
                    var params = {};
                    params.vendor = VENDER_WXA;
                    params.js_code = res.code;
                    API.APIAuthSocial.authSocial(params).then(d => {
                        var openid = d.data.openid;
                        getApp().globalData.openid = openid;
                        wx.setStorageSync('o',openid);

                        wx.setStorageSync('t',d.data.token);
                        getApp().globalData.token = d.data.token;
                        var userInfo = d.data.user;
   
                        var wxUserInfo = that.data.userInfo;                                                                 
                        userInfo.nickname = wxUserInfo.nickName
                        var photo = {};
                        photo.thumb = wxUserInfo.avatarUrl;
                        photo.large = wxUserInfo.avatarUrl;
                        userInfo.photo = photo;
                        userInfo.gender = wxUserInfo.gender;
                        wx.setStorageSync('u',userInfo);
                        getApp().globalData.userInfo = userInfo;

                         var params = {};
                        params.nickname = userInfo.nickname;                
                        params.gender = userInfo.gender;

                        API.APIUser._update(params).then(d=>{
                            wx.navigateBack({
                                delta: 1
                            });
                        });

                    })
                }
            });
    },
    */

    // signin
    tapSignin: function(e) {
      if (Util.trim(this.data.inputUsername).length == 0) {
        getApp().showTips('请输入正确的用户名或手机号');
        return;
      }
      if (this.data.inputPassword.length < 6 || this.data.inputPassword.length > 20) {
        getApp().showTips('请输入6-20位数字/字母/字符的密码');
        return;
      }

      wx.login({
              success: function(res) {
                  var VENDER_WXA = 5;
                  var params = {};
                  params.vendor = VENDER_WXA;
                  params.js_code = res.code;
                  API.APIAuthSocial.authSocial(params).then(d => {
                      var openid = d.data.openid;
                      getApp().globalData.openid = openid;
                      wx.setStorageSync('o',openid);
                  }).catch(e=>{
                    console.log(e);
                  })
              }
          });

      var data = this.data;
      var that = this;
      var params = {};
      params.username = data.inputUsername;
      params.password = data.inputPassword;
      
      app.showLoading();
      API.APIAuthBase.signin(params).then(d => {      
        app.hideLoading();          
              var data = that.data;
              app.globalData.userInfo = d.data.user;
              app.globalData.token = d.data.token;
              wx.setStorageSync('u', d.data.user);
              wx.setStorageSync('t', d.data.token);
              wx.navigateBack({
                  delta: 2
              });
              data.loading = false;
          })
          .catch(e => {
            app.hideLoading();
              data.loading = false;
              app.showTips(e);
          })
    },


    tapWxaSignin:function(e){
      wx.showToast({
          title: '加载中',
          icon: 'loading',
          duration: 60000
      });
    },
});
