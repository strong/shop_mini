// pages/favorite/index.js

var common = require('../../utils/util.js');

var API = require('../../api/api.endpoint.js');

var startX;
var startY;
var endX;
var endY;
var key;
var maxRight = 60;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    products: [],
    isEmpty: false,
  },

  reloadData: function() {
    const that = this;

    var params = {};
    params.page = 1;
    params.per_page = 1000;
    wx.showLoading({
      title: '加载中...'
    });
    API.APIFavorite.getFavoriteList(params).then(d => {
      wx.stopPullDownRefresh();
      wx.hideLoading();
      for (var i in d.data.products) {
        var data = d.data.products[i];
        data.startRight = 0;
        data.right = 0;
      }
      that.setData({
        products: d.data.products,
        isEmpty: d.data.products.length <= 0
      })
    }).catch(e => {
      wx.stopPullDownRefresh();
      wx.hideLoading();
      getApp().showTips(e);
    })
  },


  drawStart: function (e) {
    var touch = e.touches[0];
    startX = touch.clientX;
    startY = touch.clientY;
    var products = this.data.products;
    for (var i in products) {
      var data = products[i];
      data.startRight = data.right = 0;
    }

    key = true;
  },

  productTap: function(e){
    var productId = parseInt(e.currentTarget.id);
    var url = '../productDetail/index?pid=' + productId;
    wx.navigateTo({
      url: url
    });
  },

  drawEnd: function (e) {
    var products = this.data.products;
    for (var i in products) {
      var data = products[i];
      if (data.right <= 100 / 2) {
        data.right = 0;
      } else {
        data.right = maxRight;
      }
    }
    this.setData({
      products: products
    });
  },

  drawMove: function (e) {
    var self = this;
    var dataId = e.currentTarget.id;
    var products = this.data.products;
    if (key) {
      var touch = e.touches[0];
      endX = touch.clientX;
      endY = touch.clientY;
      console.log("startX=" + startX + " endX=" + endX);
      if (endX - startX == 0)
        return;
      var res = products;
      
      if ((endX - startX) < 0) {//从右往左
        for (var k in res) {
          var data = res[k];
          if (data.id == dataId) {
            var startRight = data.startRight;
            var change = startX - endX;
            startRight += change;
            if (startRight > maxRight) {
              startRight = maxRight;
            }
            data.right = startRight;
          }
        }
      } else {//从左往右  
        for (var k in res) {
          var data = res[k];
          if (data.id == dataId) {
            var startRight = data.startRight;
            var change = endX - startX;
            startRight -= change;
            if (startRight < 0) {
              startRight = 0;
            }
            data.right = startRight;
          }
        }
      }
      self.setData({
        products: products
      });

    }
  },

  //删除item  
  delItem: function (e) {
    var that = this;

    var params = {};
    var dataId = e.target.dataset.id;
    params.product = dataId;
    wx.showLoading({
      title: '加载中...'
    });
    API.APIProduct.unlikeProduct(params).then(d => {
      wx.hideLoading();
      
      var products = that.data.products;
      var newProducts = [];
      for (var i in products) {
        var data = products[i];
        if (data.id != dataId) {
          newProducts.push(data);
        }
      }
      this.setData({
        products: newProducts,
        isEmpty: newProducts.length <= 0
      });
      
    }).catch(e => {
      wx.hideLoading();
      getApp().showTips(e);
    })
  },

  goShopping: function () {
    wx.switchTab({
      url: '../index/index',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let data = this.data;
    this.setData(data);

    // this.reloadData();
    wx.startPullDownRefresh();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.reloadData();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.reloadData();
  },
})