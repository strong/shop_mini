var API = require('../../api/api.endpoint.js');
var common = require('../../utils/util.js');
Page({
    data: {
      articleId: 0,
      articles: [],
      isEmpty: false,
    },

    onLoad:function(e){
      var that = this;
      var data = this.data;
      if (e.article) {
        data.articleId = e.article;
      };
      that.setData(data);
      that.getArticleList(data.articleId);
    },

    //通过ID获取文章列表
    getArticleList: function (e) {
      var that = this;
      var data = this.data;

      var params = {};
      params.id = e;
      params.page = 1;
      params.per_page = 1000;
      wx.showLoading({
        title: '加载中...'
      });
      API.APIArticleList.getArticleList(params).then(d=>{
        wx.hideLoading();

        data.articles = d.data.articles;
        data.isEmpty = d.data.articles.length <= 0;

        this.setData(data);

      }).catch(e => {
        wx.hideLoading();
      });
    },

    //选择帮助列表item
    selectArticle:function(e){
      var that = this;
      var data = this.data;
      var selectArticle = data.articles[parseInt(e.currentTarget.id)];
      if (selectArticle.more) {
        var url = '../helpList/index?article=' + selectArticle.id;
        wx.navigateTo({
          url: url
        });
      } else {
        if (selectArticle.url.indexOf('https') < 0) {
          selectArticle.url = selectArticle.url.replace('http', 'https');
        }
        var url = '../webView/index?url=' + selectArticle.url;
        wx.navigateTo({
          title: selectArticle.title,
          url: url
        });
      }
    }
});
