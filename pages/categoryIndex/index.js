var CONFIG = require('../../config/config.js');
var app = getApp();
var common = require('../../utils/util.js');
var RED = require('../common/red.js');

//获取应用实例
var API = require('../../api/api.endpoint.js');
Page({
  data: {

    inputFocus: false,
    inputValue: "",
    title: '加载中...',
    loading: false,
    api_host: CONFIG.API_HOST,

  },

  onLoad: function(options) {
    console.log('page onLoad');
    //this.reload();options.id
    //this.tapWxaSignin();
    var that = this;
    var CONFIG = require('../../config/config.js');
    wx.request({

      url: CONFIG.API_HOST + "/wx2/category_index.php?id=" + options.id,
      method: 'POST',
      data: {
        name: 'qinziheng',
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },

      success: function(res) {
        // console.log(res.data)
        that.setData({
          categories: res.data.data.categories
        });
        that.setData({
          arr_ad1: res.data.data.arr_ad1
        });
        that.setData({
          cat_detail: res.data.data.cat_detail
        });
        that.setData({
          currentItemId: options.id
        });
        that.setData({
          all_cates: res.data.data.all_cates
        })

        // if(res.data.data.arr_ad1.length > 0 ){
        //   wx.setNavigationBarTitle({
        //     title: res.data.data.arr_ad1[0].ad_name + "频道"
        //   });
        // }
        let cid = options.id;
        if (res.data.data.categories[cid] != undefined){
          wx.setNavigationBarTitle({
            title: res.data.data.categories[cid].name + "频道"
          });
        }

        wx.hideNavigationBarLoading();
        //console.log(res.data.all_cates)
      }
    })

    //  console.log(app.globalData.openid);
    wx.showNavigationBarLoading();
    RED.checkRed(this);
  },

  tapMenuItem: function(e) {

    this.setData({
      currentItemId: e.currentTarget.id
    })
  },

  /*tapName: function(event) { 
   console.log(event) ;
    wx.pageScrollTo({
    scrollTop: event-800,
    duration: 300
  })
    },*/


  tapProductList: function(e) {
    var CategoryId = parseInt(e.currentTarget.id);
    var url = '../productList/index?cid=' + CategoryId;
    wx.navigateTo({
      url: url
    });
  },
  tapProduct: function (e) {
    var productId = parseInt(e.currentTarget.id);
    var productIdTuan = parseInt(e.currentTarget.dataset.act_id);
    var url = '../productDetail/index?pid=' + productId;
    var urlTuan = '../group/detail?pid=' + productId + "&act_id=" + productIdTuan;
    if (productIdTuan == 0) {
      wx.navigateTo({
        url: url
      });
    } else {
      wx.navigateTo({
        url: urlTuan
      });
    }
  },


  tapAd: function(e) {
    var url = String(e.currentTarget.id);
    wx.navigateTo({
      url: url
    });
  },
  bindInput: function(e) {
    this.setData({
      inputFocus: true,
      inputValue: e.detail.value
    });
  },

  tapCancel: function(e) {
    this.setData({
      inputFocus: false,
      inputValue: ""
    });
  },
  bindChange: function(e) {
    var url = "../searchResult/index?keyword=" + this.data.inputValue;
    wx.navigateTo({
      url: url
    });
  },
  //关闭遮盖
  closeImg: function(e) {
    RED.closeImg(this)
  },
  //显示大的红包
  showView: function() {
    RED.showView(this)
  },
  //领取红包
  getRed: function(e) {
    RED.getRed(this)
  },

})