var API = require('../../api/api.endpoint.js');
//获取应用实例
var app = getApp()
Page( {
  /*
   // 订单数量统计
        "ORDER_SUBTOTAL" :
        {
             "created"                : 5                            // 待付款
           , "paid"                   : 5                            // 已付款 待发货
           , "delivering"             : 5                            // 发货中
           , "deliveried"             : 5                            // 已发货，待评价
           , "finished"               : 5                            // 已完成
           , "cancelled"              : 5                            // 已取消
        },
  */
  data: {
    userInfo: {},
    showName:"点击登录",
    userAvatarUrl: '',
    showCashgift: false,
    orderSubtotal:{ // 订单数量统计
      created: 0,    // 待付款
      paid: 0,      // 已付款 待发货
      delivering: 0,    // 待收货
      deliveried: 0,    // 待评价
      finished: 0,    // 已完成
      cancelled: 0, // 已取消
    }
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo( {
      url: '../logs/logs'
    })
  },
  touchLogin:function(){
    wx.navigateTo({
      url: '../authorize/index'
    })
  },
  onLoad: function() {
    var data = this.data;
    var that = this;
    var token = wx.getStorageSync('t')

    if (app.globalData && app.globalData.userInfo && app.globalData.userInfo.photo) {
      var userAvatar = app.globalData.userInfo.photo;
      data.userAvatarUrl = userAvatar.large ? userAvatar.large : userAvatar.theme;
      that.setData(data);
    }

    if (app.globalData && app.globalData.config && app.globalData.config.feature && app.globalData.config.feature.cashgift ) {
      this.data.showCashgift = app.globalData.config.feature.cashgift;
      that.setData(data);
    }
    return {
      title: '给你发送了一个大红包',
      path: 'pages/index/index?id=5',
      imageUrl: 'https://shop.y-med.com.cn/images/receipt/hongbao.png'
    }
  },

     //下拉刷新
  onPullDownRefresh: function(){
       this.getUserInfo();
       this.getOrderSubtotal();
       wx.stopPullDownRefresh()  
  },

  //每次可见都获取用户资料
  onShow:function(){
      this.getUserInfo();
      this.getOrderSubtotal();
      app.updateCartCount();
      var that = this
      var t = wx.getStorageSync('t');
      if(t){
        var user = wx.getStorageSync('u');
        
        if (user) {
          var data = {
            showName: user.nickname
          };
          var num = 0;
          Object.keys(user).forEach(function (key) {
            num++;
          });
          
          if(user.avatar){
            data.userAvatarUrl = user.avatar;
          }
          that.setData(data)
        }
      }else{
        wx.setStorageSync('u', '');
      }
  },

  getUserInfo: function(){
    var data = this.data;
    var that = this;
    var token = wx.getStorageSync('t')
    if (token && token.length > 0) {    
        API.APIProfileGet.getUserProfile().then(d=>{
        that.setData({
          userInfo : d.data.user,
        });
        if(d.data.user.is_admin===1){
          that.setData({
            isShowSend:true,
          })
        }
        // if (data.userInfo.nickname) {
        //   data.showName = data.userInfo.nickname;
        // } else {
        //   data.showName = data.userInfo.username;
        // }

        var userAvatar = app.globalData.userInfo.photo;
        if (userAvatar) {
          data.userAvatarUrl = userAvatar.large ? userAvatar.large : userAvatar.theme;
        }

        that.setData(data);
      });
    }
  },

// 订单数量统计
  getOrderSubtotal: function() {
    if (!app.isLogin()) {
      return;
    }

    var that = this;
    API.APIOrder.orderSubtotal().then(d => {
      var subtotal = d.data.subtotal;
      that.setData({
        orderSubtotal: subtotal
      });
    });
  },
 /**
   * 进行页面分享
   */
  onShareAppMessage: function (options) {
    if (options.from === 'button') {
      // 来自页面内转发按钮
      console.log(options.target)
    }
    return {
      title: '给你发送了一个大红包', 
      path: 'pages/index/index?id=5', 
      imageUrl:'https://shop.y-med.com.cn/images/receipt/hongbao.png'
    }
  },
  // 检查是否登录
  checkOnline: function () {
    if (!app.isLogin()) {
      this.touchLogin();
      return false;
    }
    return true;
  },

  pushMyInfo: function () {
    if (!this.checkOnline()) {
      return;
    }
    
    wx.navigateTo({
      url: '../myInfo/index'
    })
  },

  pushAllOrderList: function () {
    if (!this.checkOnline()) {
      return;
    }

    wx.navigateTo({
      url: '../order/index?index=' + "0",
    })
  },

  // 跳转到订单列表页面
  pushOrderListCreated: function () {
    if (!this.checkOnline()) {
      return;
    }

    wx.navigateTo({
      url: '../order/index?index=' + "1",
    })
  },

  pushOrderListPaid: function () {
    if (!this.checkOnline()) {
      return;
    }

    wx.navigateTo({
      url: '../order/index?index=' + "2",
    })
  },

  pushOrderListDelivering: function () {
    if (!this.checkOnline()) {
      return;
    }

    wx.navigateTo({
      url: '../order/index?index=' + "3",
    })
  },
  pushOrderListComment: function () {
    if (!this.checkOnline()) {
      return;
    }

    wx.navigateTo({
      url: '../order/index?index=' + "4",
    })
  },

  tapListItem: function (e) {
    if (!this.checkOnline()) {
      return;
    }

    let url = e.currentTarget.dataset.url;

    // if (url.search('../addressList/index') != -1 && wx.chooseAddress) {
    //   wx.chooseAddress(); // 微信原生收货地址 
    // } else {
      wx.navigateTo({
        url: url,
      })
    // }
  },
  inapp: function(e){
   
    wx.getSystemInfo({

      success: function (res) {//model
          
          console.log(res)
      }
    })
  //   if (）{//wx.getSystemInfo
  //     wx.navigateBackApplication({
  //       "extraData": { key: "传递到app的数据" }
  //     })
  //  }else{
  //   wx.navigateBackApplication({
  //     "extraData": "传递到app的数据"
  //   })
  },

  signout: function () {
    // TODO:
    wx.clearStorageSync();
    getApp().globalData.token = null;
    getApp().globalData.userInfo = null;
    getApp().globalData.openid = null;

    this.setData({
      userInfo: {},
      showName: '点击登录',
      userAvatarUrl: '',
      isShowSend:false,
      orderSubtotal: { // 订单数量统计
        created: 0,
        paid: 0,
        delivering: 0, 
        deliveried: 0,
        finished: 0,
        cancelled: 0
      }
    })

    app.setTabBarBadge(0);
  }
})