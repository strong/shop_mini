var API = require('../../api/api.endpoint.js');
const Util = require('../../utils/util.js');

var interval;
var intervalDownCount = 0;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    index: 0,
    isSignup: true, // true注册页，false忘记密码页
    codeBtnTitle: '获取验证码',
    codeBtnDisable: false,
    inputValue1: null,
    inputValue2: null,
    inputValue3: null,
    inputValue4: null,
    ispwd: 0
  },

  // 点击切换tab
  onTabClick: function (e) {
    if (e.currentTarget.dataset.index != this.data.index) {
      this.setData({
        index: e.currentTarget.dataset.index,
        inputValue1: null,
        inputValue2: null,
        inputValue3: null,
        inputValue4: null,
      })
    }
  },

  // 获取验证码
  getVerificationCode: function (e) {
    var that = this
    if (!this.data.inputValue1 || this.data.inputValue1.length <= 0) {
      getApp().showTips('请输入手机号');
      return;
    }
    if (this.data.inputValue1.length < 11) {
      getApp().showTips('请输入11位数字手机号');
      return;
    }



    // 获取验证码接口
    var params = {};
    params.type = 'small';
    // params.code = 86;
    params.mobile = this.data.inputValue1;
    wx.showLoading({
      title: '加载中...'
    });

    // 验证手机号是否存在，成功则未注册，失败则已注册
    API.APIAuthBase.verifyPhoneNumber(params).then(s => {
      //console.log(s)
      that.setData({
        ispwd:s.data.ispwd
      })
      // 未注册
      if (this.data.isSignup) {
        API.APIAuthBase.sendPhoneVerifyCode(params).then(s => {
          wx.hideLoading();
          this.startTime();
        }).catch(e => {
          wx.hideLoading();
          wx.showToast({
            title: e,
            icon: 'none'
          })
        })
      } else {
        // 忘记密码
        getApp().showTips("手机号未注册");
      }
    }).catch(e => {
      // 已注册
      if (this.data.isSignup) {
        getApp().showTips("手机号已注册");
      } else {
        API.APIAuthBase.sendPhoneVerifyCode(params).then(s => {
          wx.hideLoading();
          this.startTime();
        }).catch(e => {
          wx.hideLoading();
          wx.showToast({
            title: e,
            icon: 'none'
          })
        })
      }
    })
  },

  startTime: function () {
    this.stopTime();
    intervalDownCount = 60;
    interval = setInterval(this.downCount, 1000);
    this.downCount(); // 定时器不会立即执行，所以先手动执行一次
  },
  stopTime: function () {
    clearInterval(interval);
  },

  downCount: function() {
    intervalDownCount--;

    if (intervalDownCount <= 0) {
      this.stopTime();
      this.setData({
        codeBtnTitle: '获取验证码',
        codeBtnDisable: false,
      })
    } else {
      this.setData({
        codeBtnTitle: '重新发送 ' + `(${intervalDownCount}s)`,
        codeBtnDisable: true,
      })
    }
  },

  // input change
  inputChange: function (e) {
    var data = {};
    if (e.currentTarget.dataset.id == 0) {
      data = { inputValue1: e.detail.value }
    } else if (e.currentTarget.dataset.id == 1) {
      data = { inputValue2: e.detail.value }
    } else if (e.currentTarget.dataset.id == 2) {
      data = { inputValue3: e.detail.value }
    } else if (e.currentTarget.dataset.id == 3) {
      data = { inputValue4: e.detail.value }
    } else if (e.currentTarget.dataset.id == 4) {
      data = { inputValue5: e.detail.value }
    }

    this.setData(data);
  },

  // signup phone tap
  tapSignupPhone: function (e) {
    
    //return ;
    let data = this.data;
    var ispwd = this.data.ispwd;
    var tips;
    var that = this;
   
    if (!data.inputValue1 || data.inputValue1.length <= 0) {
      tips = '请输入手机号';
    } else if (!data.inputValue2 || data.inputValue2.length <= 0) {
      tips = '请输入验证码';
    } else if (data.inputValue2.length != 6) {
      tips = '请输入6位验证码';
    }
    if(ispwd){
      if (!data.inputValue3 || data.inputValue3.length <= 0) {
        tips = '请输入密码';
      } else if (data.inputValue3.length < 6 || data.inputValue3.length > 20) {
        tips = '请输入6-20位数字/字母/字符的密码';
      } else if (!data.inputValue4 || data.inputValue4.length <= 0) {
        tips = '请再次输入密码';
      } else if (data.inputValue3 != data.inputValue4) {
        tips = '两次密码不一致';
      }
    }
    

    if (tips) {
      getApp().showTips(tips);
      return;
    }
    if (data.inputValue5) {
      //验证邀请码是否正确
      
      var CONFIG = require('../../config/config.js');
      wx.request({
        url: CONFIG.PARTER_HOST + "/api/group/checkNumber",//请求html
        method: 'POST',
        data: {
          numbers: data.inputValue5,
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        success: function (res) {
          if (res.data.code == 200) {
            that.signupPhone();
          }else{
            getApp().showTips(res.data.msg);
          }
        }
      })
    }else{
      this.signupPhone();
    }
    
    
    
  },

  // 手机注册
  signupPhone: function (arr) {
    //console.log('手机注册');
    var that = this;
    var user = wx.getStorageSync('u');
    var params = {};
    params.mobile = this.data.inputValue1;
    params.code = this.data.inputValue2;
    params.password = this.data.inputValue3;
    params.avatar = user.avatarUrl;
    params.nickname = user.nickName;
    //params.numbers =  this.data.inputValue5
    params.numbers = ''

    if (getApp().globalData.sharerUid) { // 如果有推荐人id，需要传推荐人id参数
      params.invite_code = getApp().globalData.sharerUid;
    }
    // wx.showLoading({
    //   title: '加载中...'
    // });

    wx.login({
      success: function (res) {
        var VENDER_WXA = 5;
        params.vendor = VENDER_WXA;
        params.js_code = res.code;
        if (getApp().globalData.sharerUid) { // 如果有推荐人id，需要传推荐人id参数
          params.invite_code = getApp().globalData.sharerUid;
        }

        API.APIAuthSocial.authSocial(params).then(d => {
          var userInfo = d.data.user;
          var wxUserInfo = user;
          userInfo.nickname = wxUserInfo.nickName
          var photo = {};
          photo.thumb = wxUserInfo.avatarUrl;
          photo.large = wxUserInfo.avatarUrl;
          userInfo.photo = photo;
          userInfo.gender = wxUserInfo.gender;
          wx.setStorageSync('u', userInfo);
          getApp().globalData.userInfo = userInfo;

          var openid = d.data.openid;

          getApp().globalData.openid = openid;
          wx.setStorageSync('o', openid);

          wx.setStorageSync('t', d.data.token);
          getApp().globalData.token = d.data.token;
          var userInfo = d.data.user;
          //console.log(userInfo)

         

          wx.switchTab({
            url: '/pages/mine/index',
          })
          
        }).catch(e => {
          app.hideLoading();
          app.showTips(e);
          wx.setStorageSync('u', '')
        })
        
      },
      fail: function (res) {
        app.hideLoading();
        app.showTips('微信登录失败');
        
      }
    });
    
  },

  // 邮箱注册
  signupEmail: function () {
    console.log('邮箱注册');
    var that = this;
    var params = {};
    params.username = this.data.inputValue1;
    params.email = this.data.inputValue2;
    params.password = this.data.inputValue3;
    if (getApp().globalData.sharerUid) { // 如果有推荐人id，需要传推荐人id参数
      params.invite_code = getApp().globalData.sharerUid;
    }
    wx.showLoading({
      title: '加载中...'
    });
    API.APIAuthBase.signupByEmail(params).then(s => {
      wx.hideLoading();

      getApp().globalData.userInfo = s.data.user;
      getApp().globalData.token = s.data.token;
      wx.setStorageSync('u', s.data.user);
      wx.setStorageSync('t', s.data.token);
      wx.navigateBack({
        delta: 2
      });
      
    }).catch(e => {
      wx.hideLoading();
      wx.showToast({
        title: e,
        icon: 'none'
      })
    })
  },

  // 手机号找回密码
  forgotPhone: function () {
    console.log('手机号找回密码');
    var params = {};
    params.mobile = this.data.inputValue1;
    params.code = this.data.inputValue2;
    params.password = this.data.inputValue3;
    wx.showLoading({
      title: '加载中...'
    });
    API.APIAuthBase.forgotByPhone(params).then(s => {
      wx.hideLoading();
      wx.showToast({
        title: '密码重置成功，请重新登录',
        icon: 'none'
      })
      wx.navigateBack({
        delta: 1
      });

    }).catch(e => {
      wx.hideLoading();
      wx.showToast({
        title: e,
        icon: 'none'
      })
    })
  },

  // 邮箱找回密码
  forgotEmail: function () {
    console.log('邮箱找回密码');
    var params = {};
    params.email = this.data.inputValue1;
    wx.showLoading({
      title: '加载中...'
    });
    API.APIAuthBase.forgotByEmail(params).then(s => {
      wx.hideLoading();
      wx.showToast({
        title: '重置密码链接已发至您的邮箱，请前往查看',
        icon: 'none'
      })

    }).catch(e => {
      wx.hideLoading();
      wx.showToast({
        title: e,
        icon: 'none'
      })
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var data = this.data;

    data.isSignup = options.type == 'signup';

    this.setData(data);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
    
      wx.setNavigationBarTitle({
        title: '绑定手机号',
      })
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },
})