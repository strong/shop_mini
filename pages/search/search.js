// search.js
var API = require('../../api/api.endpoint.js');
var searchTitle = "searchLogList";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    keywords: [],
    isHotSearchShow: false,
    searchList: [],
    isHistorySearchShow: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.reloadSearch();

  },

  reloadSearch: function (e) {
    var that = this;

    API.APISearchProductList.getKeywordList("").then(d => {
      var data = this.data;
      data.keywords = d.data.keywords;
      if (d.data.keywords.length > 0) {
        data.isHotSearchShow = true;
      } else {
        data.isHotSearchShow = false;
      }
      that.setData(data);
    });

    var that = this;
    try {
      if ("" != wx.getStorageSync("searchLogList")) {
        that.setData({
          isHistorySearchShow: true,
          searchList: wx.getStorageSync("searchLogList")
        });
      } else {
        that.setData({
          isHistorySearchShow: false
        });
      }
    } catch (e) {

    }


  },

  tapHistorySearch: function (e) {
    var data = this.data;
    var keyword = this.data.searchList[parseInt(e.currentTarget.id)];
    var url = "../searchResult/index?keyword=" + keyword;
    wx.navigateTo({
      url: url
    });

    // // 搜索后将搜索记录缓存到本地  
    // try {
    //   if ("" != keyword) {
    //     var searchLogData = this.data.searchList;
    //     searchLogData.unshift(keyword);
    //     if (searchLogData.length > 10) {
    //       searchLogData.pop(); //移除最后一个元素
    //     }
    //     this.setData({
    //       searchList: searchLogData,
    //       isHistorySearchShow: true,

    //     });
    //     wx.setStorageSync("searchLogList", searchLogData);
    //   }
    // } catch (e) {

    // }
  },

  tapKeyword: function(e){
    var data = this.data;
    var keyword = this.data.keywords[parseInt(e.currentTarget.id)];
    var url = "../searchResult/index?keyword=" + keyword.content;
    wx.navigateTo({
      url: url
    });

    // 搜索后将搜索记录缓存到本地  
    try {
      if ("" != keyword.content) {
        var issame = false;
        var searchLogData = this.data.searchList;
        
        searchLogData.map(function (item) {
          if (keyword.content == item){
            issame = true;
          }
        })
        if(!issame){
          searchLogData.unshift(keyword.content);
        }
        if (searchLogData.length > 10) {
          searchLogData.pop(); //移除最后一个元素
        }
        this.setData({
          searchList: searchLogData,
          isHistorySearchShow: true,

        });
        wx.setStorageSync("searchLogList", searchLogData);
      }
    } catch (e) {

    }

  },

  deleteTap: function (e) {
    try {
      wx.removeStorageSync('searchLogList')
    } catch (e) {
      // Do something when catch error
    }
    this.setData({
      searchList: [],
      isHistorySearchShow: false,
    });
  },


  bindInput: function (e) {
    this.setData({
      inputFocus: true,
      inputValue: e.detail.value
    });
  },
  bindBlur: function (e) {
    this.setData({
      inputFocus: false,
      inputValue: ""
    });
  },
  tapCancel: function (e) {
    this.setData({
      inputFocus: false,
      inputValue: ""
    });
  },
  bindChange: function (e) {
    var data = this.data;
    data.inputValue = data.inputValue.replace(/\s+/g, '') ;
    if ("" == data.inputValue){
      getApp().showTips('请输入您要搜索的关键字');
    } else{
    var url = "../searchResult/index?keyword=" + data.inputValue;
    wx.navigateTo({ url: url });

    // 搜索后将搜索记录缓存到本地  
    try {
      if ("" != data.inputValue) {
        var searchLogData = data.searchList;
        var issame = false;
        searchLogData.map(function (item) {
          if (data.inputValue == item) {
            issame = true;
          }
        })
        if (issame==false) {
          searchLogData.unshift(data.inputValue);
        }

        if (searchLogData.length > 10) {
          searchLogData.pop(); //移除最后一个元素
        }
        this.setData({
          searchList: searchLogData,
          isHistorySearchShow: true,

        });
        wx.setStorageSync("searchLogList", searchLogData);
      }
    } catch (e) {

    }
    }
    
  },
  isHistorySearchShow: function () {
    var that = this;
    try {
      if ("" != wx.getStorageSync("searchLogList")) {
        that.setData({
          isHistorySearchShow: true,
          searchList: wx.getStorageSync("searchLogList"),
        });
      } else {
        that.setData({
          isHistorySearchShow: false,
        });
      }
    } catch (e) {
      // Do something when catch error
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
})
