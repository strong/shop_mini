var API = require('../../api/api.endpoint.js');
var WxParse = require('../../libs/wxParse/wxParse.js');
const ENUM = require('../../utils/enum.js');
var CONFIG = require('../../config/config.js');
var app = getApp();
var common = require('../../utils/util.js');
var RED = require('../common/red.js');
Page({
  data: {
    // 轮播相关配置
    indicatorDots: true,
    sure: true,
    vertical: false,
    autoplay: true,
    interval: 2000,
    duration: 200,
    isTopload:false,
    cartAmount: getApp().cartTotal(),
    checkchange: true,
    showModalStatus: false,
    animationData: '',
    specnum: '数量规格选择',
    changespec: '请选择规格属性',
    firstAttr: [],
    firstPrice: [],
    threeList:[],
    iscan:false,

    //tab配置
    tabs: [{ name: "商品介绍", active: true }, { name: "规格参数", active: false }],

    //商品数量
    product: '',
    productAmount: 1,
    swiperCurrent:1,
    isPromos: false,
    promos: '',
    isLike: false,
    productReviews: '',

    // 相关配件 参数
    accessoryProduct: '',
    isRecommendShow: false,
    dotArray: '',
    dotPosition: 0,
    // 相关商品 参数
    recommendProduct: '',
    isProductShow: false,
    productDotArray: '',
    productDotPosition: 0,
    showView: false,//getApp().globalData.showView
    api_host: CONFIG.API_HOST,
    commmon_paket: false,
    sure:false,
    num: true,
    is_cart_amount:0,
    img_url: CONFIG.API_HOST,
    show: false,
  },
  choose: function () {
    var checkchange = this.data.checkchange;
    this.setData({
      checkchange: !checkchange
    })
  },
  // 相关商品
  swiperChangeProduct: function (e) {
    var video = wx.createVideoContext('video');
    video.pause();
    var position = 0;
    if (e.detail.source == "touch") {
      if(e.detail.current>=1){
        this.setData({
          checkchange:false,
        })
      }
      if (e.detail.current >= 4) {
        position = 2;
      } else if (e.detail.current >= 1) {
        position = 1;
      } else if (e.detail.current >= 0) {
        position = 0;
      }
      this.setData({
        productDotPosition: position
      });

    }
  },
  // 相关配件 
  swiperChange: function (e) {
    var position = 0;
    if (e.detail.source == "touch") {
     
      if (e.detail.current >= 4) {
        position = 2;
      } else if (e.detail.current >= 1) {
        position = 1;
      } else if (e.detail.current >= 0) {
        position = 0;
      }
      this.setData({
        dotPosition: position,
      });

    }
  },


  onShareAppMessage: function () {
    var query = `?pid=${this.data.product.id}&uid=${app.globalData.userInfo.id}`;
    return {
      title: this.data.product.name,
      path: 'pages/productDetail/index' + query
    }
  },

  //选中第一个属性
  onLoad: function (option) {
    
    var data = this.data;
    data.productId = option.pid;
    //data.showView = getApp().globalData.showView
    data.loading = true;
    this.setData(data);
    this.setClick(option.pid);
    RED.checkRed(this);
  },
  setClick:function(id){
    let params = {goods_id:id};
    if(app.isLogin()){
      var u = wx.getStorageSync('u');
      params.uid = u.id;
    }
    API.APIProduct.browseNumber(params).then(res =>{
    })

  },
  onPageScroll:function(e){
    if(e.scrollTop>wx.getSystemInfoSync().windowHeight){
      this.setData({
        isTopload:true
      })
    }else{
      this.setData({
        isTopload:false
      })
    }
  },
  gotop:function(){
    wx.pageScrollTo({
      scrollTop:0
    })
  },
  //下拉刷新
  onPullDownRefresh: function () {
    var data = this.data;
    if (getApp().globalData.cartData) {
      data.cartAmount = getApp().globalData.cartData.cartTotal;
    }
    this.reloadProduct();
    this.loadCart();

    this.setData(data);
    wx.stopPullDownRefresh()
  },

  onShow: function (option) {
    //商品页面可见时，获取商品数据和购物车数据
    var data = this.data;
    if (getApp().globalData.cartData) {
      data.cartAmount = getApp().globalData.cartData.cartTotal;
    }
    this.reloadProduct();
    
    this.loadCart();

    this.setData(data);
  },
  //去首页
  toHome: function(){
    if (app.isLogin()) {
      var url = '../index/index';
      wx.switchTab({
        url: url
      })
    }
    else {
      app.login();
    }
  },
  reloadProduct: function () {
    wx.showToast({
      title: '加载中',
      icon: 'loading',
      duration: 10000
    });
    var params = {};
    params.product = this.data.productId;

    API.APIProduct.getProduct(params).then(d => {
      
      var that = this;
      var data = this.data;
      var product = d.data.product;
      product.yuanjia_price = product.current_price;
      if (product.photos == '' || product.photos.length == 0) {
        var photo = new Object();
        photo.large = '';
        product.photos = new Array(photo);
      }
      if (!product.goods_video) {
        that.setData({
          checkchange: false
        })
      }else{
        var threeList =[];
        threeList.push(product.photos[0]);
        product.photos.map(item =>{
          threeList.push(item);
        })
        product.photos = threeList;
      }
      let attr = product.properties //商品规格
      let len = attr.length
      if(len == 1){
        let child = attr[0].attrs.length
        if(child == 1){
          this.setData({
            specnum: attr[0].attrs[0].attr_name,
            changespec: attr[0].attrs[0].attr_name,
          })
          product.properties[0].attrs[0].selected = true
          product.properties[0].attrs[0].notClick = true
        }
      }
      
      data.product = product;
      this.setupProductData(product);

      WxParse.wxParse('goods_desc', 'html', data.product.goods_desc, that, 5);
      data.product.properties.map(function (item) {
        // 不对多选判断
        if (!item.is_multiselect) {
          item.attrs.map(function (property) {
            var properties = [property.id];

            if (that.propertyExistence(properties)) {
              property.noClick = false;
            }
            else {
              property.noClick = true;
            }
          })
        }
      });
      
      data.loading = false;
      data.show = true;
      that.setData(data);
      
      wx.hideToast();
    });

    var request_params = {};
    request_params.product = this.data.productId;
    request_params.grade = 0;
    request_params.per_page = 2;
    request_params.page = 1;
    // 获取商品评价
    API.APIProduct.getReviews(request_params).then(d => {
      var that = this;
      var data = this.data;
      data.productReviews = d.data.reviews;

      if (data.productReviews.length > 0) {
        data.productReviews.map(function (item) {
          if (item.is_anonymous == 1) {
            item.author.username = common.formatNameAnony(item.author.username);
          }
          item.author.avatar  = item.author.avatar;
          item.created_at = common.formatTimeLine(item.created_at);
          item.updated_at = common.formatTimeLine(item.updated_at);

        })

      }


      that.setData(data);
    });


    // 获取相关配件
    var accessory_params = {};
    accessory_params.product = this.data.productId;
    accessory_params.per_page = 9;
    accessory_params.page = 1;

    API.APIProduct.getAccessoryProduct(accessory_params).then(d => {
      var that = this;
      var data = this.data;
      this.data.accessoryProduct = d.data.products;
      if (d.data.products.length > 0) {
        data.isRecommendShow = true;

      } else {
        data.isRecommendShow = false;
      }
      if (d.data.products.length > 6) {
        data.dotArray = [1, 2, 3];
      } else if (d.data.products.length > 3) {
        data.dotArray = [1, 2];
      } else if (d.data.products.length > 0) {
        data.dotArray = [1];
      } else {
        data.dotArray = '';
      }

      that.setData(data);
    });

    // 获取相关商品
    var recomment_params = {};
    recomment_params.product = this.data.productId;
    recomment_params.per_page = 9;
    recomment_params.page = 1;
    recomment_params.shop = this.data.product.shop;

    API.APIProduct.getRecommentProduct(recomment_params).then(d => {
      var that = this;
      var data = this.data;
      data.recommendProduct = d.data.products;
      if (d.data.products.length > 0) {
        data.isProductShow = true;

      } else {
        data.isProductShow = false;
      }
      if (d.data.products.length > 6) {
        data.productDotArray = [1, 2, 3];
      } else if (d.data.products.length > 3) {
        data.productDotArray = [1, 2];
      } else if (d.data.products.length > 0) {
        data.productDotArray = [1];
      } else {
        data.productDotArray = '';
      }

      that.setData(data);
    });



  },

  // 设置商品数据
  setupProductData: function (product) {
    var islike = false;
    var promoStr = '';
    if (product.promos.length) {
      this.isPromos = true;

      for (var promoIndex in product.promos) {
        var promo = product.promos[promoIndex];

        if (promo.promo && promo.promo.length) {
          promoStr = promoStr + '，' + promo.promo;
        }
      }

      if (promoStr && promoStr.length) {
        promoStr = ' ' + promoStr.substring(1, promoStr.length) + ' ';
      }
    }
    else {
      this.isPromos = false;
    }
    if (1 == product.is_liked) {
      islike = true;
    }
    var isDotShow = true;
    if (product.photos.length <= 1) {
      isDotShow = false;
    } else {
      isDotShow = true;
    }

    this.setData({
      promos: promoStr,
      isPromos: this.isPromos,
      isLike: islike,
      indicatorDots: isDotShow
    })

    return product;
  },

  commentTap: function (e) {
    var url = '../comment/index?pid=' + this.data.product.id;
    wx.navigateTo({
      url: url
    });
  },

  // 相关
  swiperItemTap: function (e) {
    var productId = parseInt(e.currentTarget.id);
    var url = '../productDetail/index?pid=' + productId;
    wx.navigateTo({
      url: url
    });
  },

  relativeProductTap: function (e) {
    var url = '../relativeProduct/index?title=相关商品&pid=' + this.data.product.id + '&shop=' + this.data.product.shop;
    wx.navigateTo({
      url: url
    });
  },

  relativeAccssoryTap: function (e) {
    var url = '../relativeProduct/index?title=相关配件&pid=' + this.data.product.id;
    wx.navigateTo({
      url: url
    });
  },

  // 收藏
  likeTap: function (e) {
    wx.showToast({
      title: '',
      icon: 'loading',
      duration: 10000
    });
    var params = {};
    params.product = this.data.productId;
    if (this.data.isLike) {
      API.APIProduct.unlikeProduct(params).then(d => {
        var that = this;
        var data = this.data;
        data.isLike = d.data.is_liked;
        // data.product = this.setupProductData(d.data.product);



        data.loading = false;
        that.setData(data);
        wx.hideToast();
        getApp().showTips('取消收藏');
      });
    } else {
      API.APIProduct.likeProduct(params).then(d => {
        var that = this;
        var data = this.data;
        data.isLike = d.data.is_liked;
        // data.product = this.setupProductData(d.data.product);



        data.loading = false;
        that.setData(data);
        wx.hideToast();
        getApp().showTips('收藏成功');
      });
    }

  },



  //切换属性
  propertyTap: function (e) {
    var that = this;
    var data = this.data;
    var name = e.currentTarget.dataset.obj;
    var selectedItem;
    data.product.properties.map(function (_item) {
      if (name === _item.name) {
        selectedItem = _item;
      }
    })
    
    var currentProperty = selectedItem.attrs[parseInt(e.currentTarget.id)];
    
    if (currentProperty.noClick || currentProperty.notClick) {
      return; // 不可点击的属性，不处理点击事件（因为不可点击的属性，UI上虽然做了置灰处理，但是仍然可以点击，所以代码还要限制一下）
    }
    //console.log(currentProperty)

    // 只把单选框的置空
    if (!selectedItem.is_multiselect) {
      selectedItem.attrs.map(function (property) {

        // 忽略不可点击的
        if (currentProperty.noClick) {
          return;
        }

        // 把除了选中的子属性以外的其它属性都设置为未选中
        if (!(currentProperty.id == property.id)) {

          property.selected = false;
        }
      })
    }

    // 如果当前属性已经选择，那么当再次点击的时候，就取消选择
    currentProperty.selected = !currentProperty.selected;

    //价格联动处理
    var bigattr = selectedItem.id;
    var firstAttr = that.data.firstAttr;
    var firstPrice = that.data.firstPrice;
    var new_price = that.data.new_price ? that.data.new_price : that.data.product.current_price;

    if (!that.in_array(bigattr, firstAttr)) { //判断当前大规格id是否存在于firstAttr数组中，如果不存在，那么直接原价格加当前点击的价格
      var price = that.data.product.current_price;
      firstPrice[bigattr] = currentProperty.attr_price;
      firstAttr = firstAttr.push(bigattr);
      var end_price = parseFloat(new_price, 2) + parseFloat(currentProperty.attr_price, 2)
      that.setData({
        new_price: end_price.toFixed(2),
        firstPrice: firstPrice
      })
    } else {//如果存在那么判断当前操作是 选择当前规格？还是取消当前规格？
      if (currentProperty.selected == false) { //currentProperty.selected == false 是取消当前规格，那么把显示的价格减去当前规格的价格
        var new_price = that.data.new_price ? that.data.new_price : that.data.product.current_price;
        var end_price = parseFloat(new_price, 2) - currentProperty.attr_price;
        firstPrice[bigattr] = 0;
        for (var i = 0; i < firstAttr.length; i++) { //循环大规格数组，去掉当前要取消的大规格id
          if (firstAttr[i]) {
            if (firstAttr[i] == bigattr) {
              firstAttr.splice(i, 1)
            }
          }
        }

        that.setData({
          new_price: end_price.toFixed(2),
          firstPrice: firstPrice,
          firstAttr: firstAttr
        })
      } else {//currentProperty.selected == false 是选中当前规格，那么把显示的价格加选中的价格，再减去上一次选择的大规格
        firstAttr = firstAttr.push(bigattr);

        if (new_price == that.data.product.current_price) {//判断当前新价格是否和原始价格一致，如果一直直接添加属性价格，反之，需要减去上一次添加的价格
          var end_price = parseFloat(new_price, 2) + parseFloat(currentProperty.attr_price, 2);
        } else {
          var end_price = parseFloat(new_price, 2) + parseFloat(currentProperty.attr_price, 2) - parseFloat(firstPrice[bigattr], 2);
        }

        firstPrice[bigattr] = currentProperty.attr_price; //记录当前添加的价格，方便下次做减操作
        that.setData({
          new_price: end_price.toFixed(2),
          firstPrice: firstPrice
        })
      }

    }

    // 处理多属性的组合库存
    let arr = [];
    data.product.properties.map(function (_item) {
      //console.log(_item)
      for (let i = 0; i < _item.attrs.length; i++) {
        arr.push(_item.attrs[i])
      }
      // 排除多选框
      if (!_item.is_multiselect) {
        /*  
        // 排除已经选择的
        var isSelected = false

        for (var attrIndex in _item.attrs) {
          var property = _item.attrs[attrIndex];

          // 用当前选中的属性和别的属性进行组合，然后去库存中对比，不符合的，设置为不可点击
          var selectedProperties = that.currentSelectedProperties()

          for (var propertySelectedIndex in selectedProperties) {
            if (selectedProperties[propertySelectedIndex] == property.id) {
              isSelected = true;
            }
          }
        }

        if (!isSelected) {
          for (var attrIndex in _item.attrs) {
            var property = _item.attrs[attrIndex];

            // 用当前选中的属性和别的属性进行组合，然后去库存中对比，不符合的，设置为不可点击
            var selectedProperties = that.currentSelectedProperties()

            selectedProperties.push(property.id);
            // 把当前出了选中的属性的父属性以外  进行组合
            if (that.propertyExistence(selectedProperties)) {
              property.noClick = false;
            } else {
              property.noClick = true;
            }
          }
        }
        */
        // 上面注释的这段判断逻辑有问题，

        if (_item.id != selectedItem.id) { // 如果是当前选的属性就不处理，只判断当前选的属性和其他属性的组合库存
          _item.attrs.map(property => {
            //console.log(property)
            // 用当前选中的属性和别的属性进行组合，然后去库存中对比，不符合的，设置为不可点击
            var selectedProperties = [property.id];
            if (currentProperty.selected) {
              selectedProperties.push(currentProperty.id)
            }
            // 把当前出了选中的属性的父属性以外  进行组合
            if (that.propertyExistence(selectedProperties)) {
              property.noClick = false;
            } else {
              property.noClick = true;
            }
          })
        }
      }
    })
    const atr = [];
    for (let j = 0; j < arr.length; j++) {
      //console.log(arr[j]);

      if (arr[j].selected) {
        //atr.push(arr[j].attr_name + '(加￥' + arr[j].attr_price + ')')
        atr.push(arr[j].attr_name)
      }
    }
    if (atr.length == 0) {
      this.setData({
        specnum: '数量规格选择',
        changespec: '请选择规格属性',
      })
    } else if (atr.length == 1) {
      this.setData({
        specnum: atr[0],
        changespec: atr[0],
      })
    } else if (atr.length == 2) {
      this.setData({
        specnum: atr[0] + atr[1],
        changespec: atr[0] + atr[1],
      })
    }
    this.setData(data);
  },
  //swiperChang handle
  swiperchange: function (e) {
    //FIXME: 当前页码
    //console.log(e.detail.current)
    if(e.detail.current>=1){
      this.setData({
        checkchange:false,
        swiperCurrent: e.detail.current + 1 //获取当前轮播图片的下标
      })
    }else if(e.detail.current==0){
      this.setData({
        checkchange:true,
        swiperCurrent: 1 //获取当前轮播图片的下标
      })
    }
  },

  //tab切换
  tapTab: function (e) {
    var data = this.data;
    data.tabs.map(function (tab, index) {
      tab.active = false;
    })
    data.tabs[parseInt(e.currentTarget.id)].active = true;
    this.setData(data);
  },

  //添加数量
  tapAdd: function (e) {
    var nowAmount = this.data.productAmount;
    nowAmount++;

    var amount = this.currentProductAmount();

    if (amount != null) {
      if (amount >= nowAmount) {
        this.setData({
          productAmount: nowAmount
        });
      }
      else {
        this.setData({
          productAmount: amount
        });

        getApp().showTips('库存不足');
      }
    } else {
      getApp().showTips('请选择商品属性');

    }


  },
  //减少数量
  tapSub: function (e) {
    var nowAmount = this.data.productAmount;

    if (nowAmount > 1) {
      this.setData({
        productAmount: --nowAmount
      });
    }
  },

  //填写数量
  bindInput: function (e) {

    var amount = this.currentProductAmount();
    if (amount != null) {
      if (amount >= e.detail.value) {
        this.setData({
          productAmount: e.detail.value
        });

      }
      else {
        this.setData({
          productAmount: amount ? amount : 1
        });
        getApp().showTips('库存不足');

      }
    } else {
      this.setData({
        productAmount: amount ? amount : 1
      });
      getApp().showTips('请选择商品属性');

    }

  },

  // 获取当前选择的属性
  currentSelectedProperties: function () {
    // 获取当前选中属性的库存，只获取单选框的，多选框忽略
    var propertySelected = [];
    this.data.product.properties.map(function (_item) {
      if (!_item.is_multiselect) {
        // 不为多选框
        _item.attrs.map(function (property) {
          if (property.selected) {
            propertySelected.push(property.id);
          }
        })
      }
    })

    return propertySelected;
  },

  // 获取当前库存
  currentProductAmount: function () {
    // 如果没有属性，只有数量，那么就获取商品总库存，否则获取当前所选择属性的库存

    if (this.data.product.properties.length) {
      var item = this.currentSelectedPropertie();
      if (item) {
        return item.stock_number;
      } else {
        return null;
      }
    }
    else {
      return this.data.product.good_stock;
    }
  },

  // 获取当前选择的属性库存对象
  currentSelectedPropertie: function () {
    var propertySelected = this.currentSelectedProperties();

    // 判断选中子属性的数组是否在库存数组中，在的话取出对应属性库存对象
    var currentSelectedItem;
    
    for (var stockIndex in this.data.product.stock) {
      var item = this.data.product.stock[stockIndex];

      var propertySelectedStr = propertySelected.sort().toString();

      var itemPropertyStr = item.goods_attr.split("|").sort().toString();

      if (propertySelectedStr == itemPropertyStr) {
        return item;
      }
    }

    return currentSelectedItem;
  },

  // 根据的到的属性id数组，来判断是否存在与库存组合中
  propertyExistence: function (propertys) {

    var that = this;
    var isExistence = false;

    that.data.product.stock.map(function (_item) {
      var goodsAttrs = _item.goods_attr.split("|");
      // 判断propertys中元素是否存在与库存中
      if (that.isSubset(goodsAttrs, propertys)) {
        isExistence = true;
      }
    })

    return isExistence;
  },

  //如果arr2是arr1的子集，则返回1
  isSubset: function (arr1, arr2) {
    var numCount = 0;

    for (var arr2Index in arr2) {
      for (var arr1Index in arr1) {
        if (arr1[arr1Index] == arr2[arr2Index]) {
          numCount++;
        }
      }
    }

    if (numCount >= arr2.length && numCount > 0)
      return 1;
    else
      return 0;
  },
  //关闭弹框
  backClick: function (e) {
    var animation = wx.createAnimation({
      duration: 200,
      timingFunction: "linear",
      delay: 0
    })
    this.animation = animation
    animation.translateY(300).step()
    this.setData({
      animationData: animation.export(),
    })
    setTimeout(function () {
      animation.translateY(0).step()
      this.setData({
        animationData: animation.export(),
        showModalStatus: false
      })
    }.bind(this), 200)
  },
  // 加入购物车判断
  tapAddCart: function (e) {
    var that = this; 
    if (app.isLogin()) {
      var amount = this.currentProductAmount();
      // console.log(this.data.is_cart_amount);
      if (amount != null && this.data.is_cart_amount == 0) {
        this.data.is_cart_amount = 1;
        if (0 >= amount) {
          getApp().showTips('库存不足'); 
          return;
        }

        // 判断当前组合是否存在
        if (that.data.product.properties && that.data.product.properties.length) {
          if (!that.propertyExistence(that.currentSelectedProperties())) {
            getApp().showTips('当前组合不存在');

            return;
          }
        }

        wx.showToast({
          title: '加载中',
          icon: 'loading',
          duration: 30000
        });

        var params = {};
        params.product = this.data.productId;
        params.amount = this.data.productAmount;
        var propertySelected = [];

        this.data.product.properties.map(function (_item) {
          _item.attrs.map(function (property) {
            if (property.selected) {
              propertySelected.push(property.id);
            }
          })
        })

        params.property = JSON.stringify(propertySelected);
        API.APICart._add(params).then(d => {
          // d.data;
          getApp().showTips('加入购物车成功');
          
          that.loadCart();
          var animation = wx.createAnimation({
            duration: 200,
            timingFunction: "linear",
            delay: 0
          })
          this.animation = animation
          animation.translateY(300).step()
          this.setData({
            animationData: animation.export(),
          })
          setTimeout(function () {
            animation.translateY(0).step()
            this.setData({
              animationData: animation.export(),
              showModalStatus: false
            })
          }.bind(this), 200)
        }).catch(e => {
          getApp().showTips(e);
        });

      } else {
        this.data.is_cart_amount = 0;
        getApp().showTips('请选择商品属性');
        var animation = wx.createAnimation({
          duration: 200,
          timingFunction: "linear",
          delay: 0
        })
        this.animation = animation
        animation.translateY(300).step()
        this.setData({
          animationData: animation.export(),
          showModalStatus: true
        })
        setTimeout(function () {
          animation.translateY(0).step()
          this.setData({
            animationData: animation.export()
          })
        }.bind(this), 200)

      }
    } else {
      app.login();
    }
  },
  loadCart: function () {
    if (!app.isLogin()) {
      return; // 如果没有登录，则不刷新购物车，该接口需要登录
    }

    var that = this;
    var data = this.data;
    API.APICart._get().then(d => {
      getApp().globalData.cartData = d.data.goods_groups[0];

      if (getApp()) {
        data.cartAmount = getApp().cartTotal();
      }

      that.setData(data);
    });
  },
  tapBuyNow: function (e) {
    if (this.data.productAmount == 1 && this.data.changespec == '选择 参数') {
      var animation = wx.createAnimation({
        duration: 200,
        timingFunction: "linear",
        delay: 0
      })
      this.animation = animation
      animation.translateY(300).step()
      this.setData({
        animationData: animation.export(),
      })
      setTimeout(function () {
        animation.translateY(0).step()
        this.setData({
          animationData: animation.export()
        })
      }.bind(this), 200)
    }
    var that = this;
    if (app.isLogin()) {
      var amount = this.currentProductAmount();
      if (amount != null) {
        if (0 >= amount) {
          getApp().showTips('库存不足');
          return;
        }

        // 判断当前组合是否存在
        if (that.data.product.properties && that.data.product.properties.length) {
          if (!that.propertyExistence(that.currentSelectedProperties())) {
            getApp().showTips('当前组合不存在');
            return;
          }
        }
        
        getApp().globalData.confirmProductData.product = this.data.product;

        var propertySelected = [];
        this.data.product.properties.map(function (_item) {
          _item.attrs.map(function (property) {
            if (property.selected) {
              propertySelected.push(property.id);
            }

          })
        });
        getApp().globalData.confirmProductData.attrs = propertySelected;
        getApp().globalData.confirmProductData.amount = this.data.productAmount;

        var url = '../confirmOrder/index?type=' + ENUM.CONFIRM_PRODUCT;
        wx.navigateTo({
          url: url
        });
      } else {
        getApp().showTips('请选择商品属性');
        var animation = wx.createAnimation({
          duration: 200,
          timingFunction: "linear",
          delay: 0
        })
        this.animation = animation
        animation.translateY(300).step()
        this.setData({
          animationData: animation.export(),
          showModalStatus: true
        })
        setTimeout(function () {
          animation.translateY(0).step()
          this.setData({
            animationData: animation.export()
          })
        }.bind(this), 200)
      }
    }
    else {
      app.login();
    }
  },
  //点击规格参数的弹框
  openModal: function (e) {
    var animation = wx.createAnimation({
      duration: 200,
      timingFunction: "linear",
      delay: 0
    })
    this.animation = animation
    animation.translateY(300).step()
    this.setData({
      animationData: animation.export(),
      showModalStatus: true
    })
    setTimeout(function () {
      animation.translateY(0).step()
      this.setData({
        animationData: animation.export()
      })
    }.bind(this), 200)
  },
  touchCart: function (e) {
    if (app.isLogin()) {
      var url = '../cart/index';
      wx.switchTab({
        url: url
      })
    }
    else {
      app.login();
    }
  },
  in_array: function (search, array) {
    for (var i in array) {
      if (array[i] == search) {
        return true;
      }
    }
    return false;
  },
  //关闭遮盖
  closeImg: function (e) {
    RED.closeImg(this)
  },
  //显示大的红包
  showView: function () {
    RED.showView(this)
  },
  //领取红包
  getRed: function (e) {
    RED.getRed(this)
  },
})
