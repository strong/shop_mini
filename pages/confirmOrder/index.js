var common = require('../../utils/util.js');
var API = require('../../api/api.endpoint.js');
var USERAPI = require('../../api/api.user.js');
var RED = require('../common/red.js');
var PAY = require('../common/pay.js');
const ENUM = require('../../utils/enum.js');
var CONFIG = require('../../config/config.js');
const app = getApp();
Page({
  data: {
    //默认地址
    defaultAddress: '',
    //无地址时
    showAddress: false,
    total_price: 60,
    cashgift: '', //选中的红包
    cashgiftDesc: "未选中红包",
    showInvoice: false, //是否显示发票，当无发票内容是，就不显示发票选项
    invoiceType: {}, //发票类型ID
    invoiceContent: {}, //发票内容
    invoiceTitle: "", //发票抬头内容
    noInvoice: true, //是否不要发票 
    merchantCashgift: "0.00", //商家红包金额
    active_price: "0.00",
    comment: "",
    all_discount: 0, //总共的优惠金额
    shipping: "", //选择的快递方式
    showCashgift: true,
    isLoading: false,
    noUseCash: false,
    isSubmit: true, //当前是否可以提交 
    selectedScore: '', // 要使用的抵扣积分
    selectedScorePrice: '0.00', // 使用抵扣积分抵扣的金额
    usableScore: 0, // 用户可用积分
    productUsableScore: 0, // 订单商品限制使用的积分,
    is_number: false,
    is_group: 0,
    act_id: 0,
    product_price: 0,
    is_red: 1,
    show: false,
    add_show: false,
    image: [],
    img_count: 4,
    img_url: CONFIG.API_HOST,
    type: 'success',
  },

  /**
   * 组件的属性列表
   */
  properties: {
    count: { //最多选择图片的张数，默认9张
      type: Number,
      value: 4
    },
    uploadUrl: { //图片上传的服务器路径
      type: String,
      value: ''
    },
    showUrl: { //图片的拼接路径
      type: String,
      value: ''
    }
  },

  onLoad: function(options) {
    var data = this.data;
    var that = this;
    if (options.act_id) {
      data.act_id = options.act_id
    }
    if (options.is_group) {
      data.is_group = options.is_group
    }
    if (options.group_id) {
      data.group_id = options.group_id
    }
    if (options.is_number){
      data.is_number = true
    }
    //onLoad的时候将已选中的快递方式制为null
    getApp().globalData.confirmOrderData.shippingVender = "";
    //进入页面时，获取地址列表，找到默认地址
    //调用应用实例的方法获取全局数据

    if (app.globalData && app.globalData.config && app.globalData.config.feature) {
      this.data.showCashgift = app.globalData.config.feature.cashgift;
      // this.data.showInvoice = app.globalData.config.feature.invoice;
    }

    // this.reloadAddress(); //在onShow中执行
    this.getInvoiceContent();
    data.confirmType = options.type;
    this.initData(data.confirmType);
    that.setData(data);
    //var u = wx.getStorageSync('u');
    // if (!u.numbers || u.numbers == '') {
    //   this.setData({
    //     is_number: 1
    //   })
    // } else {
    //   this.setData({
    //     is_number: 0
    //   })
    // }

    // 获取当前积分
    API.APIScore.getCurrentScore().then(d => {
      that.setData({
        usableScore: d.data.score
      })
    })


  },


  // 上传图片
  uploadDetailImage: function (e) { //这里是选取图片的方法
    var index = 1;//e.currentTarget.id;
    console.log(index)
    var that = this;
    wx.chooseImage({
      count: that.data.img_count, // 最多可以选择的图片张数，默认4
      sizeType: ['original', 'compressed'], // original 原图，compressed 压缩图，默认二者都有
      sourceType: ['album', 'camera'], // album 从相册选图，camera 使用相机，默认二者都有
      success: function (res) {
        var tempFilePaths = res.tempFilePaths
        
        for (var i = 0; i < tempFilePaths.length; i++) {
          var image = that.data.image
          var arr = image == undefined ? [] : image;
          if (image === undefined || image.length < that.data.img_count) {

            wx.uploadFile({
              url: CONFIG.API_HOST + '//v2/ecapi.order.uploads', //仅为示例，非真实的接口地址
              filePath: tempFilePaths[i],
              name: 'image',
              header: {
                "Content-Type": "json"
              },
              dataType: 'json',
              success: function (res) {
                if (res.data != 0) {
                  arr.push(res.data)
                  image = arr
                  that.setData({
                    image: image
                  })
                }
              }
            })

          } else {
            wx.showModal({
              title: '提示',
              showCancel: 'false',
              content: '您选择超过了' + that.data.img_count + '张图片，已为您选择你优先选择的图片'
            })
          }
        } 
        console.log(image)
      }
    })
  }, 
  //删除图片
  delimg: function (e) {
    var index = e.currentTarget.dataset.index;
    var image = this.data.image;
    image.splice(index, 1)
    this.setData({
      image: image
    })
  },

  //下拉刷新
  onPullDownRefresh: function() {
    wx.stopPullDownRefresh()
  },

  initData: function(type) {
    var data = this.data;
    var that = this;
    // console.log(getApp().globalData.confirmProductData.product)
    if (type == ENUM.CONFIRM_PRODUCT) {
      data.goods = [];
      if (getApp().globalData.confirmProductData.product) {
        
        var card_good = {};
        card_good.product = getApp().globalData.confirmProductData.product;
        
        var attrs = getApp().globalData.confirmProductData.attrs;
        card_good.property = "";
        card_good.attrs = [];
        if (data.is_group == 1) {
          var product_price = parseFloat(card_good.product.current_price);
        } else {
          var product_price = parseFloat(card_good.product.yuanjia_price);
        }


        var attrsLength = attrs.length;
        for (var i = 0; i < attrsLength; i++) {

          var propertiesLength = card_good.product.properties.length;
          for (var j = 0; j < propertiesLength; j++) {

            var property = card_good.product.properties[j];
            var length = property.attrs.length;
            for (var k = 0; k < length; k++) {
              var attrItem = property.attrs[k];

              if (parseInt(attrItem.id) == attrs[i]) {
                if (card_good.property.length > 0) {
                  card_good.property += "," + attrItem.attr_name;
                } else {
                  card_good.property = attrItem.attr_name;
                }
                card_good.attrs.push(attrItem.id);
                if (data.is_group == 1) {
                  product_price += parseFloat(attrItem.attr_group_price);
                } else {
                  product_price += parseFloat(attrItem.attr_price); //attr_group_price
                }

              }
            }
          }
        }
        card_good.amount = getApp().globalData.confirmProductData.amount;
        card_good.price = product_price;


        data.goods.push(card_good);
        getApp().globalData.confirmOrderData.goods = data.goods;
      }
    } else {
      data.goods = getApp().globalData.confirmOrderData.goods;

      data.goods.map(good => {
        if (good.property && good.property.length > 0) {
          good.property = good.property.replace(/[\r\n]/g, ",");

          var propertys = good.property.split(",");
          var property = '';
          propertys.map((p, i) => {
            var ps = p.split(":");
            if (ps.length == 2) {
              property += common.trim(ps[1]);
              if (i != propertys.length - 2) {
                property += ','
              }
            }
          })
          if (property.length > 0) {
            good.property = property;
          }
        }
      })
    } 
    data.goods.map(d => {
      if (d.product) {
        data.productUsableScore += d.product.score * d.amount;
      }
    })
    // console.log(data);
    this.setData(data);
  },
  //默认选中规格最大的红包
  defaultRedBonus: function(price) {
    var that = this
    var params = {};
    params.page = 1;
    params.per_page = 10000000;
    params.total_price = price;
    var goods = this.data.goods
    if (this.data.confirmType == 0){
      var goods_ids = goods[0].product.id
    }else{
      var goods_ids = '';
      for (var i = 0; i < goods.length; i++) {
        if(i>0){
          goods_ids += ','
        }
        goods_ids += goods[i].goods_id
      }
    }
    if(goods_ids){
      params.goods_ids = goods_ids;
      this.setData({
        goods_ids: goods_ids
      })
    }
    //params.goods_id = 
    API.APICashgiftAvailable.getCashgiftAvailable(params).then(d => {
      if (d.data.paged.total > 0) {
        let redbonus = d.data.cashgifts[0]
        var data = {
          cashgift: redbonus,
          cashgiftDesc: redbonus.value + "元",
          cashgiftyDesc: redbonus.value + "元",
          index: 0,
          is_red: 0
        }

        that.setData(data)
        that._reloadPrice();
      } else {
        var desc = '暂时没有红包可用';
        that.setData({
          cashgiftDesc: desc
        })
      }

    });
  },
  gored: function(e) {
    var cashgift = this.data.cashgift;
    var goods_ids = this.data.goods_ids;
    if (!this.data.act_id) {
      var url = "../cashgift/index?total_price=" + this.data.order_price.product_price
      if (cashgift) {
        url += "&id=" + cashgift.id
      }
      if(goods_ids){
        url += '&goods_ids=' + goods_ids
      }
      // console.log(url);
      // return ;
      wx.navigateTo({
        url: url,
      })
    }
  },
  setShipping: function() {
    var data = this.data;
    var that = this;
  // console.log(data)
    if (data.shipping == '' && data.defaultAddress != '') {
      var products = [];
      var goods = getApp().globalData.confirmOrderData.goods;
      for (var key in goods) {
        var good = goods[key];
        var shoppingProduct = {
          goods_id: good.product.id,
          num: good.amount
        };
        products.push(shoppingProduct);
      };
      var params = {};
      params.address = this.data.defaultAddress.id;
      params.products = JSON.stringify(products);
      API.APIShipping.list(params).then(d => {
        var shipping = {}
        shipping = d.data.vendors[0];
        that.setData({
          shipping,
        }, () => that._reloadPrice())
      }).catch(e => {

      });
    }
  },
  scoreInputChange: function(e) { // 输入积分
    var score = e.detail.value;

    var usableScore = parseInt(this.data.usableScore) > parseInt(this.data.productUsableScore) ? parseInt(this.data.productUsableScore) : parseInt(this.data.usableScore);

    if (parseInt(score) > usableScore) {
      score = String(usableScore);
    }
    this.setData({
      selectedScore: score
    })
  },

  scoreInputDidChanged: function(e) { // 输入积分结束,计算价格
    var score = e.detail.value;
    this.setData({
      selectedScore: score
    })

    this._reloadPrice();
  },
  //设置激活码
  setNumber: function(e) {
    var that = this
    var numbers = e.detail.value;
    if (!numbers) {
      return;
    }
    var u = wx.getStorageSync('u');
    var that = this

    var CONFIG = require('../../config/config.js');
    wx.request({
      url: CONFIG.PARTER_HOST + "/api/group/checkNumber", //请求html
      method: 'POST',
      data: {
        numbers: numbers,
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: function(res) {
        if (res.data.code == 200) {
          //绑定用户激活码
          var params = {};
          params.id = u.id;
          params.numbers = numbers
          API.APIUser.setNumber(params).then(d => {
            u.numbers = numbers;
            wx.setStorageSync('u', u)
            that.setData({
              is_number: 0
            }, () => that._reloadPrice())


          });

        } else {
          getApp().showTips(res.data.msg);
        }
      }
    })

  },

  // 进入收货地址页面
  pushAddressList: function() {
    //       if (wx.chooseAddress) {
    //         let that = this;
    //         wx.chooseAddress({
    //           success: function(res) {
    //             console.log(res);
    //             /*
    // cityName : "广州市"
    // countyName : "海珠区"
    // detailInfo : "新港中路397号"
    // nationalCode : "510000"
    // postalCode : "510000"
    // provinceName : "广东省"
    // telNumber : "020-81167888"
    // userName : "张三"
    // */
    //             var data = that.data;
    //             data.defaultAddress = {};
    //             data.defaultAddress.name = res.userName;
    //             data.defaultAddress.mobile = res.telNumber;
    //             data.defaultAddress.totalAddress = res.provinceName + res.cityName + res.countyName + res.detailInfo;
    //             data.showAddress = true;
    //             that.setData(data);
    //           },
    //           fail: function (e) {
    //             console.log(e);
    //             data.showAddress = false;
    //             that.setData(data);
    //           }
    //         })
    //       } else {
    var data = this.data;
    wx.navigateTo({
      url: '../addressList/index?consigneeId=' + data.defaultAddress.id
    })
    // }
  },

  getInvoiceContent: function() {
    var that = this;
    var data = this.data;

    API.APIInvoiceContentList.getInvoiceStstua().then(d => {
      if (d.data.is_provided) {
        data.showInvoice = true;
      } else {
        data.showInvoice = false;
      }

      that.setData(data);
    });
  },

  reloadAddress: function() {
    var data = this.data;
    var that = this;
    //进入页面时，获取地址列表，找到默认地址
    //调用应用实例的方法获取全局数据
    API.APIConsignee.getConsigneeList().then(d => {

      // 判断当前数组的长度，如果为空那么就显示地址为空
      if (d.data.consignees.length) {
        for (var consigneesIndex in d.data.consignees) {
          var totalAddress = '';

          var consignees = d.data.consignees[consigneesIndex];

          for (var regionIndex in consignees.regions) {
            var region = consignees.regions[regionIndex];

            totalAddress += region.name;
          }

          consignees.totalAddress = totalAddress + consignees.address;
        }

        //更新数据

        //获取默认地址
        d.data.consignees.map(function(item) {
          if (data.showAddress) {

          } else {
            if (item.is_default) {
              data.defaultAddress = item;
              data.showAddress = true;
            }
          }

        });
        if (data.showAddress = true) {} else {
          data.defaultAddress = d.data.consignees[0];
          data.showAddress = true;
        }

      } else {
        //更新数据
        data.showAddress = false;
        this.setData({
          add_show:true
        })
        //data.add_show = true
      }
      that.setData({
        data
      }, () => that.setShipping());
      this._reloadPrice();
    });
  },

  bindTextAreaBlur: function(e) {
    var data = this.data;
    data.comment = e.detail.value;
    this.setData(data);
  },
  //去填写激活码页面
  touchNumber: function() {
    wx.navigateTo({
      url: '../numbers/index',
    })
  },

  touchSubmit: function() {
    
    var data = this.data;
    var that = this;
    if (data.isSubmit) {

      var comment = data.comment;
      var invoiceType = data.invoiceType;
      var invoiceTitle = data.invoiceTitle;
      var invoiceContent = data.invoiceContent;

      // var that = this;
      var goods = this.data.goods;
      var consignee = this.data.defaultAddress;
      var express = this.data.shipping;
      var coupon = this.data.coupon;
      var cashgift = this.data.cashgift;
      var score = this.data.selectedScore;

      var goodsIds = [];

      if (!goods || !goods.length) {
        getApp().showTips('商品信息不存在');
        return;
      }

      for (var i = 0; i < goods.length; ++i) {
        goodsIds.push(goods[i].id);
      }

      if (!goodsIds || !goodsIds.length) {
        getApp().showTips('商品信息不存在');
        return;
      }

      if (!consignee) {
        getApp().showTips('请填写地址');
        return;
      }

      if (!express) {
        getApp().showTips('请选择配送方式');
        return;
      }

      var params = {
        shop: 1,
        consignee: consignee ? consignee.id : null,
        cart_good_id: goodsIds ? JSON.stringify(goodsIds) : null,
        shipping: 11,
        invoice_type: invoiceType ? invoiceType.id : null,
        invoice_title: invoiceTitle,
        invoice_content: invoiceContent ? invoiceContent.id : null,
        coupon: coupon ? coupon.id : null,
        cashgift: cashgift ? cashgift.id : null,
        score: score ? score : 0,
        comment: comment ? comment : ""
      };

      data.isSubmit = false;
      this.setData(data);
      // if (!wx.getStorageSync('u')['numbers'] || wx.getStorageSync('u')['numbers']=='') {
      //   params.is_number = 0;
      // } else {
      //   params.is_number = 1;
      // }
      params.is_number = this.data.is_number;
      if (data.act_id) {
        params.act_id = data.act_id;
        params.is_group = data.is_group;
        params.group_id = data.group_id;

      }
      //保存上传图片
      if (data.image){
        //上传图片到服务器 返回str
        var imgs_str = that.uploadimg(data.image); 
        params.image = imgs_str;
      }
      
      // console.log(params)
      // return;

      if (data.confirmType == ENUM.CONFIRM_PRODUCT) {
        for (var key in this.data.goods) {
          var good = this.data.goods[key];
          params.product = good.product.id;
          params.property = JSON.stringify(good.attrs);
          params.amount = good.amount;
        }
        app.showLoading();
        API.APIProduct.purchase(params)
          .then(function(result) {
            app.hideLoading();
            var order = result.data.order;
            if (order) {
              // 如果订单状态是待发货，那么就跳转到支付成功页面
              if (order.status == 1) {
                var url = '../paySuccess/index?orderID=' + order.id;
                wx.redirectTo({
                  url: url
                });
              } else {
                PAY.payTap(that,order.id,1);
                // var url = '../payment/index?id=' + order.id;
                // wx.redirectTo({
                //   url: url
                // });
                // wx.showModal({
                //   title: '支付备货定金',
                //   content: '订单已生成，是否立即支付备货定金',
                //   success: function (res) {
                //     if (res.confirm) {
                //       var url = '../payment/index?id=' + order.id;
                //       wx.redirectTo({
                //         url: url
                //       });
                //     } else {
                //       // console.log('用户点击取消')

                //       wx.redirectTo({
                //         url: '../order/index?index=' + "1",
                //       })
                //     }

                //   }
                // });
              }
            }
          }).catch(e => {
            app.hideLoading();
            data.isSubmit = true;
            this.setData(data);
            getApp().showTips(e);
          });
      } else {
        app.showLoading();
        API.APICart.checkout(params)
          .then(function(result) {
            app.hideLoading();
            var order = result.data.order;
            if (order) {
              // 如果订单状态是待发货，那么就跳转到支付成功页面
              if (order.status == 1) {
                var url = '../paySuccess/index?orderID=' + order.id;
                wx.redirectTo({
                  url: url
                });
              } else {
                PAY.payTap(that, order.id, 1);
                // getApp().globalData.confirmOrderData = {};
                // var url = '../payment/index?id=' + order.id;
                // wx.redirectTo({
                //   url: url
                // });
                // wx.showModal({
                //   title: '去支付',
                //   content: '订单已生成，是否立即支付',
                //   success: function (res) {
                //     if (res.confirm) {
                //       var url = '../payment/index?id=' + order.id;
                //       wx.redirectTo({
                //         url: url
                //       });
                //     } else {
                //       // console.log('用户点击取消')

                //       wx.redirectTo({
                //         url: '../order/index?index=' + "1",
                //       })
                //     }
                //   }
                // });
              }
            }
          }).catch(e => {
            app.hideLoading();
            data.isSubmit = true;
            this.setData(data);
          });
      }
    }
  },
  // 上传图片方法
  uploadimg: function (image) {
    var str = image.join(',');
    return str;
  },
  //全选
  select_r: function () {
    var data = this.data;
    var type = this.data.type; 
    if (type == 'circle') {
      data.type = 'success'; 
      this.setData(data);
    } else {
      data.type = 'circle'; 
      this.setData(data);
    }; 
  },
  gozhiqing: function (e) { 
    var url = "../knowing/index"; 
    wx.navigateTo({
      url: url,
    }) 
  },

  _reloadPrice: function() {
    var that = this;
    var goods = this.data.goods;
    var consignee = this.data.defaultAddress;
    var express = this.data.shipping;
    var coupon = this.data.coupon;
    var cashgift = this.data.cashgift;
    var score = this.data.selectedScore;

    if (!goods || !goods.length) {
      getApp().showTips('商品信息不存在');
      return;
    }

    if (!consignee) {
      this.setData({show:true});
      return;
    }

    var products = [];
    if (this.data.confirmType == ENUM.CONFIRM_ORDER) {
      for (var i = 0; i < goods.length; ++i) {
        products.push({
          goods_id: goods[i].product.id,
          property: goods[i].attrs.split(','),
          num: goods[i].amount
        });
      }
    } else {
      for (var key in this.data.goods) {
        var good = this.data.goods[key];
        var shoppingProduct = {
          goods_id: good.product.id,
          property: good.attrs,
          num: good.amount,
          total_amount: good.amount
        };
        products.push(shoppingProduct);
      }
    }


    var params = {};

    params.order_product = JSON.stringify(products);

    if (consignee && consignee.id) {
      params.consignee = consignee.id;
    }

    if (express) {
      params.shipping = express.id;
    }

    if (cashgift) {
      params.cashgift = cashgift.id;
    }

    if (coupon) {
      params.coupon = coupon.id;
    }

    if (score) {
      params.score = score;
    }
    // if (!wx.getStorageSync('u')['numbers'] || wx.getStorageSync('u')['numbers']=='') {
    //   params.is_number = 0;
    // } else {
    //   params.is_number = 1;
    // }
    // console.log(this.data.is_number)
    // return ;
    params.is_number = this.data.is_number;
    params.act_id = this.data.act_id
    
    API.APIOrder.price(params)
      .then(function(result) {
        var data = that.data;
        var all_discount = 0;
        data.order_price = result.data.order_price;
        data.product_price = result.data.order_price.product_price

        if (that.data.is_red && !that.data.act_id) {
          that.defaultRedBonus(data.order_price.product_price)
        }
        //data.active_price = result.data.order_price.articve_price;
        result.data.order_price.promos.forEach(function(item, index) {
          if (item.promo == "cashgift") {
            data.merchantCashgift = item.price;
          };
          if (item.promo == "score") {
            data.selectedScorePrice = item.price;
          }
          if (item.promo == "preferential") {
            data.active_price = item.price;
          }



          all_discount = parseFloat(all_discount) + parseFloat(item.price);
        })
        // for(var promo in result.data.order_price.promos){

        //     if (promo.promo == "cashgift") {
        //         data.merchantCashgift = promo.price;
        //     }
        //     data.all_discount += parseFloat(result.data.promos[promo].price);
        // }
        data.all_discount = all_discount;
        data.show = true;
        that.setData(data);

      });
  },

  _reloadScore: function() {

    $scope.maxUseScore = 0;

    for (var i = 0; i < $scope.goods.length; ++i) {
      $scope.maxUseScore += $scope.goods[i].product.score * $scope.goods[i].amount;
    }

    API.score
      .get({})
      .then(function(info) {
        $scope.scoreInfo = info;
        $scope.refreshScore();
      })
  },

  touchShipping: function() {
    if (!this.data.defaultAddress.id || this.data.defaultAddress.id.length == 0) {
      app.showTips('请选择收货地址');
      return;
    }

    var url = '../shipping/index?id=' + this.data.defaultAddress.id;
    wx.navigateTo({
      url: url
    });
  },

  //当页面可见时，就获取选中的红包信息
  onShow: function() {
    var data = this.data;
    var that = this;

    // if (!wx.chooseAddress) {
    that.reloadAddress(); // 不支持原生收货地址时加载
    // }


    if (data.cashgift) {
      data.cashgiftDesc = data.cashgift.value + "元"
      data.cashgiftyDesc = data.cashgift.value + "元"
    } else {
      data.cashgiftDesc = '未选中红包';
      // if (data.noUseCash) {
      //   data.cashgiftDesc = '不使用红包';
      // }
      //data.cashgiftDesc = '不使用红包';
    }
    
    
    
   
    that.data.shipping = getApp().globalData.confirmOrderData.shippingVender;
    that.setData(data);
    that._reloadPrice();
  }
})