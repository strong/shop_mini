// pages/relativeProduct/index.js
var API = require('../../api/api.endpoint.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    products: '',
    productId:'',
    title:'',
    shop:'',
    hasMore:false,

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var data = this.data;
    data.productId = options.pid;
    data.title = options.title;
    if (options.shop){
    data.shop = options.shop;
    }
    data.loading = true;
    this.setData(data);
    wx.setNavigationBarTitle({
      title: data.title
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  //下拉刷新
  onPullDownRefresh: function () {
    var data = this.data;
    if (getApp().globalData.cartData) {
      data.cartAmount = getApp().globalData.cartData.cartTotal;
    }
    this.reloadProduct();
    this.loadCart();

    this.setData(data);
    wx.stopPullDownRefresh()
  },
  handleLoadMore: function (e) {
    if (!this.data.hasMore) return

    var data = this.data;

    if (data.allowLoadMore) {
      data.allowLoadMore = false;
    }
    else {
      return;
    }

    data.loading = true;

    if (this.data.title == "相关配件") {
      // 获取相关配件
      var accessory_params = {};
      accessory_params.product = this.data.productId;
      accessory_params.per_page = 10;
      accessory_params.page = data.products.length / 10 + 1;

      API.APIProduct.getAccessoryProduct(accessory_params).then(d => {
        var that = this;
        var data = this.data;
        this.data.products = d.data.products;

        data.loading = false;
        that.setData(data);

        wx.hideToast();

      }).catch(e => {
        data.loading = false;
        this.setData(data);
        console.error(e)
        data.allowLoadMore = true;
      });
    } else {

      // 获取相关商品
      var recomment_params = {};
      recomment_params.product = this.data.productId;
      recomment_params.per_page = 10;
      recomment_params.page = data.products.length / 10 + 1;
      recomment_params.shop = this.data.shop;

      API.APIProduct.getRecommentProduct(recomment_params).then(d => {
        var that = this;
        var data = this.data;
        data.products = d.data.products;
        data.loading = false;
        that.setData(data);

        wx.hideToast();
      })
        .catch(e => {
          data.loading = false;
          this.setData(data);
          console.error(e)
          data.allowLoadMore = true;
        });
    }

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.reloadProduct();
  },
  reloadProduct: function () {

    if (this.data.title == "相关配件") {
      // 获取相关配件
      var accessory_params = {};
      accessory_params.product = this.data.productId;
      accessory_params.per_page = 10;
      accessory_params.page = 1;

      API.APIProduct.getAccessoryProduct(accessory_params).then(d => {
        var that = this;
        var data = this.data;
        this.data.products = d.data.products;
        data.hasMore = d.data.paged.more;

        data.loading = false;
        that.setData(data);

        wx.hideToast();
        
      }).catch(e => {
        data.loading = false;
        this.setData(data);
        console.error(e)
        data.allowLoadMore = true;
      });
    } else {

      // 获取相关商品
      var recomment_params = {};
      recomment_params.product = this.data.productId;
      recomment_params.per_page = 10;
      recomment_params.page = 1;
      recomment_params.shop = this.data.shop;

      API.APIProduct.getRecommentProduct(recomment_params).then(d => {
        var that = this;
        var data = this.data;
        data.products = d.data.products;
        data.hasMore = d.data.paged.more;
        data.loading = false;
        that.setData(data);

        wx.hideToast();
      }).catch(e => {
        data.loading = false;
        this.setData(data);
        console.error(e)
        data.allowLoadMore = true;
      });
    }

  },

  tapProduct: function(e){
    var data = this.data;
    var product = this.data.products[parseInt(e.currentTarget.id)];
    var url = '../productDetail/index?pid=' + product.id;
    wx.navigateTo({
      url: url
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.reloadProduct();

    this.setData(data);
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
})