// pages/comment/index.js
var API = require('../../api/api.endpoint.js');
var common = require('../../utils/util.js');
var CONFIG = require('../../config/config.js');
//获取应用实例
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    productId:'',
    titles:["全部","好评","中评","差评"],
    productReviews:'',
    grade:0,
    currentIndex:0,
    api_host: CONFIG.API_HOST+'/',
  },

  tapComment: function (e){
    var data = this.data;
    data.currentIndex = e.currentTarget.id;
    if (e.currentTarget.id == 3){
      data.grade = 1;
    } else if (e.currentTarget.id == 1) {
      data.grade = 3;
    } else {
      data.grade = e.currentTarget.id;
    }
    data.loading = true;
    this.setData(data);
    this.reloadComment();
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var data = this.data;
    data.productId = options.pid;
    data.loading = true;
    this.reload(0);
    this.reload(1);
    this.reload(2);
    this.reload(3);
    this.reloadComment();
    this.setData(data);
  },
  reload:function(e){
    var request_params = {};
    request_params.product = this.data.productId;
    request_params.grade = e;
    request_params.per_page = 10;
    request_params.page = 1;
    // 获取商品评价
    API.APIProduct.getReviews(request_params).then(d => {
      var that = this;
      var data = this.data;
      console.log(data.titles)
      if(e==0){
        data.titles[0]='全部('+d.data.paged.total+')';
      }else if(e==1){
        data.titles[3]='差评('+d.data.paged.total+')';
      }else if(e==2){
        data.titles[2]='中评('+d.data.paged.total+')';
      }else if(e==3){
        data.titles[1]='好评('+d.data.paged.total+')';
      }
      that.setData(data);
    });
  },
  reloadComment: function(e){
    
    var request_params = {};
    request_params.product = this.data.productId;
    request_params.grade = this.data.grade;
    request_params.per_page = 10;
    request_params.page = 1;
    // 获取商品评价
    API.APIProduct.getReviews(request_params).then(d => {
      var that = this;
      var data = this.data;
      this.data.productReviews = d.data.reviews;
      if (d.data.reviews.length > 0) {
        d.data.reviews.map(function (item) {
          if (item.is_anonymous ==1){
            item.author.username = common.formatNameAnony(item.author.username);
          }
          item.created_at = common.formatTimeLine(item.created_at);
          item.updated_at = common.formatTimeLine(item.updated_at);
          
        })

        // that.setData({
        //   isCommentShow: true
        // });

      } else {
        // that.setData({
        //   isCommentShow: false
        // });
      }
      data.loading = false;
      that.setData(data);

      wx.hideToast();

    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },big: function (e) {
    var imgarr = e.currentTarget.dataset.imgarr;
    

    for (var i = 0; i < imgarr.length; i++) {
      imgarr[i] = this.data.api_host + imgarr[i]
    }
    var index = e.currentTarget.dataset.index;
    console.log(index)
    wx.previewImage({
      current: imgarr[index], // 当前显示图片的http链接
      urls: imgarr // 需要预览的图片http链接列表
    })
  }
})