var common = require('../../utils/util.js');
var API = require('../../api/api.endpoint.js');
const ENUM = require('../../utils/enum.js');
const app = getApp();

Page({
  data: {
    //购物车是否为空
    show: false,
    //商品全选状态
    type: 'circle',

    //总价格
    totalPrice: 0,

    //总数量
    totalAmount: 0,
    currentId: -1,

    //二次确认隐藏
    modalHidden: true,
    modalHiddenAll: true,
    //选中的数组
    selectedArr: [],

    //删除的商品的id
    deleteItem: '',
    items: [],

    isLogin: true,
    showModalStatus: false,//规格弹框
    specnum: '数量规格选择',
    changespec: '请选择规格属性',
    productAmount: 1,
    firstAttr: [],
    firstPrice: [],
    sure: true,
    num: false
  },

  //加载页面时给每个选中添加状态
  onLoad: function () {

  },

  //下拉刷新
  onPullDownRefresh: function () {
    if (app.isLogin()) {
      this.getCartList();
    } else {
      this.showEmpty();
    }

    wx.stopPullDownRefresh()
  },

  onShow: function () {
    if (app.isLogin()) {
      this.getCartList();
    } else {
      this.showEmpty();
    }

    this.setData({
      isLogin: app.isLogin(),
      currentId:-1
    })
  },

  showEmpty: function () {
    this.setData({
      show: true,
      type: 'circle',
      totalPrice: 0,
      totalAmount: 0,
      modalHidden: true,
      modalHiddenAll: true,
      selectedArr: [],
      deleteItem: '',
      items: [],
    })
  },

  setTabBarBadge: function (count) {
    if (!app.isLogin() || count <= 0 || count == undefined) {
      if (wx.removeTabBarBadge) {
        wx.removeTabBarBadge({
          index: 2,
        })
      }
    } else {
      if (wx.setTabBarBadge) {
        wx.setTabBarBadge({
          index: 2,
          text: count > 99 ? '99+' : String(count),
        })
      }
    }
  },
  goBills() {
    wx.navigateTo({
      url: '/pages/collectBills/index',
    })
  },

  getCartList: function () {
    var that = this;
    wx.showToast({
      title: '加载中',
      icon: 'loading',
      duration: 10000
    });

    API.APICart._get().then(d => {
      var data = that.data;

      if (d.data.goods_groups.length) {
        var tot = 0;
        for (var i = d.data.goods_groups[0].goods.length - 1; i >= 0; i--) {
          var tempGood = d.data.goods_groups[0].goods[i];
          tempGood.type = 'circle';
          tot += parseInt(d.data.goods_groups[0].goods[i]['amount']);
        }
        d.data.goods_groups[0].total_amount = tot
      }
      data.goods_groups = d.data.goods_groups;

      var total = data.goods_groups.length ? data.goods_groups[0].total_amount : 0; // 单店，goods_groups只有一个，直接取第一个

      that.setTabBarBadge(total);

      // 如果有数据 那么就显示
      if (d.data.goods_groups.length) {
        data.show = false;
      }
      else {
        data.show = true;
      }
      data.selectedArr = [];
      data.type = 'circle';
      that.setData(data);
      that.recomputePrice();
      wx.hideToast();
      this.selectAll()
    });
  },
  //修改规格
  upAttr() {
    var product = this.data.product.properties[0].attrs;

    var goods_groups = this.data.goods_groups;

    var len = product.length
    var cart_id = this.data.cart_id; //购物车id
    var that = this
    for (let i = 0; i < len; i++) {
      if (product[i].selected) {
        var params = {}
        params.good = cart_id;
        params.attr_id = product[i].id
        let attr = this.data.product.properties[0].name + ":" + product[i].attr_name

        API.APICart._update(params).then(d => {
          var goods = goods_groups[0].goods;
          var len = goods.length
          //购物车对应的商品修改规格
          for (let i = 0; i < len; i++) {
            if (goods[i].id == cart_id) {
              goods_groups[0].goods[i].property = attr;
              that.setData({
                goods_groups: goods_groups,
                showModalStatus: false //隐藏规格选择框
              })
            }
          }
        });
      }
    }
    this.recomputePrice();



  },

  //添加数量
  tapAdd: function (e) {
    var that = this;
    var item = this.data.goods_groups[0].goods[parseInt(e.currentTarget.id)];

    //如果要修改的数量大于商品的库存数量 则弹出库存不足提示
    if (item.attr_stock < item.amount + 1) {
      wx.showToast({
        title: '库存不足'
      });

    } else {
      wx.showToast({
        title: '加载中',
        icon: 'loading',
        duration: 10000
      });
      var params = {}
      params.good = item.id;
      params.amount = item.amount + 1;
      API.APICart._update(params).then(d => {
        that.recomputePrice();
        wx.hideToast();
      });
    }

  },

  recomputePrice: function () {
    wx.showToast({
      title: '加载中',
      icon: 'loading',
      duration: 10000
    });
    var that = this;
    API.APICart._get().then(d => {
      var data = that.data;
      data.goods_groups = d.data.goods_groups;

      if (data.goods_groups.length) {
        for (var i = data.goods_groups[0].goods.length - 1; i >= 0; i--) {
          var tempGood = data.goods_groups[0].goods[i];
          tempGood.type = 'circle';
          for (var j = data.selectedArr.length - 1; j >= 0; j--) {
            var itemGood = data.selectedArr[j];
            if (tempGood.id == itemGood.id) {
              tempGood.type = 'success';
              data.selectedArr[j] = tempGood;
            }
          }

        }

        data.show = false;
      } else {
        data.show = true;
      }
      data.totalPrice = common.total(data.selectedArr).totalPrice;
      data.totalAmount = common.total(data.selectedArr).totalAmount;
      that.setData(data);

      var total = data.goods_groups.length ? data.goods_groups[0].total_amount : 0;; // 单店，goods_groups只有一个，直接取第一个
      that.setTabBarBadge(total);
      wx.hideToast();
    });
  },

  //减少数量
  tapSub: function (e) {
    var that = this;
    var data = this.data;
    if (this.data.goods_groups.length) {
      wx.showToast({
        title: '加载中',
        icon: 'loading',
        duration: 10000
      });
      var item = this.data.goods_groups[0].goods[parseInt(e.currentTarget.id)];
      if (item.amount == 1) {
        getApp().showTips('受不了了，宝贝不能再少了');
      } else {
        var params = {}
        params.good = item.id;
        params.amount = item.amount - 1;
        API.APICart._update(params).then(d => {
          that.recomputePrice();
          wx.hideToast();
        });
      }
    }
  },

  //填写数量
  bindInput: function (e) {

    if (e.detail.value.length == 0) {
      return;
    }

    var data = this.data;
    var item = this.data.goods_groups[0].goods[parseInt(e.currentTarget.id)];
    this.setData(data);

    var params = {}
    params.good = item.id;
    params.amount = e.detail.value;
    API.APICart._update(params).then(d => {
      var data = that.data;
      data.goods_groups = d.data.goods_groups;
      that.setData(data);
      that.recomputePrice();
    });
  },

  bindblur: function (e) {
    if (e.detail.value.length != 0) {
      return;
    }
    e.detail.value = 0;
    var data = this.data;
    var item = this.data.goods_groups[0].goods[parseInt(e.currentTarget.id)];
    this.setData(data);

    var params = {}
    params.good = item.id;
    params.amount = e.detail.value;
    API.APICart._update(params).then(d => {
      var data = that.data;
      data.goods_groups = d.data.goods_groups;
      that.setData(data);
      that.recomputePrice();
    });
  },

  //单个选中和取消
  tapSelect: function (e) {
    var data = this.data;
    var targetIndex = parseInt(e.currentTarget.id);
    var targetGood = this.data.goods_groups[0].goods[targetIndex];
    var type = targetGood.type;

    if (type == 'success') {
      //如果是选中状态从数组中移除
      var item = targetGood;
      var index = common.indexOf(data.selectedArr, item);
      data.selectedArr.splice(index, 1);
      targetGood.type = 'circle';
      console.log(data.selectedArr)
      console.log('select')
      this.setData(data);
    } else {
      targetGood.type = 'success';
      //如果是未选中状态添加到数组中
      data.selectedArr.push(targetGood);
      console.log(data.selectedArr);
      this.setData(data);
    }


    if (data.selectedArr.length == this.data.goods_groups[0].goods.length) {
      data.type = 'success';
      this.setData(data);
    } else {
      data.type = 'circle';
      this.setData(data);
    }

    data.totalPrice = common.total(data.selectedArr).totalPrice;
    data.totalAmount = common.total(data.selectedArr).totalAmount;
    this.setData(data);
  },

  //全选
  selectAll: function () {
    var data = this.data;
    var type = this.data.type;
    if (this.data.goods_groups.length == 0){
     return ;
    }
    if (type == 'circle') {
      data.type = 'success';
      data.selectedArr = this.data.goods_groups[0].goods;
      this.setData(data);
    } else {
      data.type = 'circle';
      data.selectedArr = [];
      this.setData(data);
    };
    data.totalPrice = common.total(data.selectedArr).totalPrice;
    data.totalAmount = common.total(data.selectedArr).totalAmount;
    this.setData(data);
    this.recomputePrice();
  },

  isSelect: function (selectItem) {
    //再购物车全部商品中删除选中的
    this.data.selectedArr.map(function (item, index) {
      {
        if (selectItem.id == item.id) {
          return true;
        }
      }
    })
    return false;
  },

  //删除二次确认
  modalTap: function (e) {
    // for (var i = 0; i < this.data.goods_groups[0].goods.length; i++) {
    //   wx.createSelectorQuery().selectAll('#+"children"+i').boundingClientRect(function (rect) {
    //     console.log(rect[0])
    //   }).exec()
    // }
    var deleteItem = this.data.goods_groups[0].goods[parseInt(e.currentTarget.id)];
    this.setData({
      modalHidden: false,
      deleteItem: deleteItem,
      index: e.currentTarget.id
      // currentId: e.currentTarget.id
    })

  },

  modalTapAll: function (e) {
    // var deleteItem = this.data.items[parseInt(e.currentTarget.id)];
    if (this.data.selectedArr.length) {
      this.setData({
        modalHiddenAll: false,
      })
    } else {
      getApp().showTips("请选择要删除的商品");
    }
  },
  //选择规格
  changeSpecif: function (e) {
    var goods = this.data.goods_groups[0].goods;
    var index = e.currentTarget.id;

    var product = goods[index].product
    if (product.properties.length == 1) { //判断该商品是否只有一个规格并且一个属性
      if (product.properties[0].attrs.length <= 1) {
        return;
      }
    }

    this.setData({
      product: product,
      cart_id: goods[index].id,
    })

    console.log(this.data.product)
    console.log(111)
    var that = this
    var attrid = goods[index].attrs;
    

    product.properties.map(function (item) {

      // 不对多选判断
      if (!item.is_multiselect) {
        item.attrs.map(function (property) {
          var properties = [property.id];
          if (property.id == attrid) {  //选中当前购物车内商品的规格
            
            property.selected = true
            var end_price = parseFloat(property.attr_price, 2) + parseFloat(product.current_price,2)
            
            

            that.setData({
              changespec: property.attr_name,
              new_price: end_price.toFixed(2)
            })
          }
          if (that.propertyExistence(properties)) {
            property.noClick = false;
          }
          else {
            property.noClick = true;
          }
        })
      }
    });
    this.setData({
      product: product,
      showModalStatus: true,
      
    })
    this.recomputePrice();
  },
  //切换属性
  propertyTap: function (e) {
    var that = this;
    var data = this.data;
    var name = e.currentTarget.dataset.obj;
    var selectedItem;
    data.product.properties.map(function (_item) {
      if (name === _item.name) {
        selectedItem = _item;
      }
    })

    var currentProperty = selectedItem.attrs[parseInt(e.currentTarget.id)];

    if (currentProperty.noClick || currentProperty.notClick) {
      return; // 不可点击的属性，不处理点击事件（因为不可点击的属性，UI上虽然做了置灰处理，但是仍然可以点击，所以代码还要限制一下）
    }
    //console.log(currentProperty)

    // 只把单选框的置空
    if (!selectedItem.is_multiselect) {
      selectedItem.attrs.map(function (property) {

        // 忽略不可点击的
        if (currentProperty.noClick) {
          return;
        }

        // 把除了选中的子属性以外的其它属性都设置为未选中
        if (!(currentProperty.id == property.id)) {

          property.selected = false;
        }
      })
    }

    // 如果当前属性已经选择，那么当再次点击的时候，就取消选择
    currentProperty.selected = !currentProperty.selected;

    //价格联动处理
    var bigattr = selectedItem.id;
    var firstAttr = that.data.firstAttr;
    var firstPrice = that.data.firstPrice;

    var new_price = that.data.product.current_price;

    if (!that.in_array(bigattr, firstAttr)) { //判断当前大规格id是否存在于firstAttr数组中，如果不存在，那么直接原价格加当前点击的价格
      var price = that.data.product.current_price;
      firstPrice[bigattr] = currentProperty.attr_price;
      firstAttr = firstAttr.push(bigattr);
      var end_price = parseFloat(new_price, 2) + parseFloat(currentProperty.attr_price, 2)
      that.setData({
        new_price: end_price.toFixed(2),
        firstPrice: firstPrice
      })
    } else {//如果存在那么判断当前操作是 选择当前规格？还是取消当前规格？
      
      if (currentProperty.selected == false) { //currentProperty.selected == false 是取消当前规格，那么把显示的价格减去当前规格的价格
        var new_price = that.data.new_price ? that.data.new_price : that.data.product.current_price;
        var end_price = parseFloat(new_price, 2) - currentProperty.attr_price;
        firstPrice[bigattr] = 0;
        for (var i = 0; i < firstAttr.length; i++) { //循环大规格数组，去掉当前要取消的大规格id
          if (firstAttr[i]) {
            if (firstAttr[i] == bigattr) {
              firstAttr.splice(i, 1)
            }
          }
        }

        that.setData({
          new_price: end_price.toFixed(2),
          firstPrice: firstPrice,
          firstAttr: firstAttr
        })
      } else {//currentProperty.selected == false 是选中当前规格，那么把显示的价格加选中的价格，再减去上一次选择的大规格
        firstAttr = firstAttr.push(bigattr);

        if (new_price == that.data.product.current_price) {//判断当前新价格是否和原始价格一致，如果一直直接添加属性价格，反之，需要减去上一次添加的价格
          var end_price = parseFloat(new_price, 2) + parseFloat(currentProperty.attr_price, 2);
        } else {
          var end_price = parseFloat(new_price, 2) + parseFloat(currentProperty.attr_price, 2) - parseFloat(firstPrice[bigattr], 2);
        }

        firstPrice[bigattr] = currentProperty.attr_price; //记录当前添加的价格，方便下次做减操作
        that.setData({
          new_price: end_price.toFixed(2),
          firstPrice: firstPrice
        })
      }

    }

    // 处理多属性的组合库存
    let arr = [];
    data.product.properties.map(function (_item) {
      //console.log(_item)
      for (let i = 0; i < _item.attrs.length; i++) {
        arr.push(_item.attrs[i])
      }
      // 排除多选框
      if (!_item.is_multiselect) {
        /*  
        // 排除已经选择的
        var isSelected = false

        for (var attrIndex in _item.attrs) {
          var property = _item.attrs[attrIndex];

          // 用当前选中的属性和别的属性进行组合，然后去库存中对比，不符合的，设置为不可点击
          var selectedProperties = that.currentSelectedProperties()

          for (var propertySelectedIndex in selectedProperties) {
            if (selectedProperties[propertySelectedIndex] == property.id) {
              isSelected = true;
            }
          }
        }

        if (!isSelected) {
          for (var attrIndex in _item.attrs) {
            var property = _item.attrs[attrIndex];

            // 用当前选中的属性和别的属性进行组合，然后去库存中对比，不符合的，设置为不可点击
            var selectedProperties = that.currentSelectedProperties()

            selectedProperties.push(property.id);
            // 把当前出了选中的属性的父属性以外  进行组合
            if (that.propertyExistence(selectedProperties)) {
              property.noClick = false;
            } else {
              property.noClick = true;
            }
          }
        }
        */
        // 上面注释的这段判断逻辑有问题，

        if (_item.id != selectedItem.id) { // 如果是当前选的属性就不处理，只判断当前选的属性和其他属性的组合库存
          _item.attrs.map(property => {
            //console.log(property)
            // 用当前选中的属性和别的属性进行组合，然后去库存中对比，不符合的，设置为不可点击
            var selectedProperties = [property.id];
            if (currentProperty.selected) {
              selectedProperties.push(currentProperty.id)
            }
            // 把当前出了选中的属性的父属性以外  进行组合
            if (that.propertyExistence(selectedProperties)) {
              property.noClick = false;
            } else {
              property.noClick = true;
            }
          })
        }
      }
    })
    const atr = [];
    for (let j = 0; j < arr.length; j++) {
      //console.log(arr[j]);

      if (arr[j].selected) {
        //atr.push(arr[j].attr_name + '(加￥' + arr[j].attr_price + ')')
        atr.push(arr[j].attr_name)
      }
    }
    if (atr.length == 0) {
      this.setData({
        specnum: '数量规格选择',
        changespec: '请选择规格属性',
      })
    } else if (atr.length == 1) {
      this.setData({
        specnum: atr[0],
        changespec: atr[0],
      })
    } else if (atr.length == 2) {
      this.setData({
        specnum: atr[0] + atr[1],
        changespec: atr[0] + atr[1],
      })
    }
    this.setData(data);
  },

  in_array: function (search, array) {
    for (var i in array) {
      if (array[i] == search) {
        return true;
      }
    }
    return false;
  },
  // 根据的到的属性id数组，来判断是否存在与库存组合中
  propertyExistence: function (propertys) {

    var that = this;
    var isExistence = false;
    that.data.product.stock.map(function (_item) {
      var goodsAttrs = _item.goods_attr.split("|");
      // 判断propertys中元素是否存在与库存中
      if (that.isSubset(goodsAttrs, propertys)) {
        isExistence = true;
      }
    })

    return isExistence;
  },

  //如果arr2是arr1的子集，则返回1
  isSubset: function (arr1, arr2) {
    var numCount = 0;

    for (var arr2Index in arr2) {
      for (var arr1Index in arr1) {
        if (arr1[arr1Index] == arr2[arr2Index]) {
          numCount++;
        }
      }
    }

    if (numCount >= arr2.length && numCount > 0)
      return 1;
    else
      return 0;
  },
  //关闭弹框
  backClick: function (e) {
    var animation = wx.createAnimation({
      duration: 200,
      timingFunction: "linear",
      delay: 0
    })
    this.animation = animation
    animation.translateY(300).step()
    this.setData({
      animationData: animation.export(),
    })
    setTimeout(function () {
      animation.translateY(0).step()
      this.setData({
        animationData: animation.export(),
        showModalStatus: false
      })
    }.bind(this), 200)
  },
  //确认删除
  confirm: function () {
    if (!app.isLogin()) {
      return;
    }
    var data = this.data;
    var that = this;
    data.modalHidden = true;
    this.setData(data);

    var index2 = common.indexOf(data.selectedArr, data.deleteItem);
    if (index2 !== -1) {
      data.selectedArr.splice(index2, 1);
      data.totalPrice = common.total(data.selectedArr).totalPrice;
      data.totalAmount = common.total(data.selectedArr).totalAmount;
      this.setData(data);
    }

    var params = {}
    params.good = data.deleteItem.id;
    var index = data.index;
    this.setData({
      currentId: index,
    })
    API.APICart._delete(params).then(d => {
      var data = that.data;
      if (d.data.goods_groups) {
        data.goods_groups = d.data.goods_groups;
      }
      that.setData(data);
      //判断是否删除完了
      if (data.goods_groups[0].goods.length == 0) {
        this.setData({
          show: true,
          type: 'circle'

        })
      }
      that.recomputePrice();
    });

  },

  //取消
  cancel: function () {
    this.setData({
      modalHidden: true
    })
  },

  touchConfirmOrder: function () {
    if (app.isLogin()) {
      if (this.data.selectedArr.length) {
        
        getApp().globalData.confirmOrderData.goods = this.data.selectedArr;
        var url = '../confirmOrder/index?type=' + ENUM.CONFIRM_ORDER;
        console.log(url)
        wx.navigateTo({
          url: url
        });
      } else {
        getApp().showTips("请选择要结算的商品");
      }
    }
  },

  confirmAll: function () {
    if (!app.isLogin()) {
      return;
    }

    wx.showToast({
      title: '加载中',
      icon: 'loading',
      duration: 10000
    });

    var data = this.data;
    var that = this;
    var params = {};
    var tempIds = [];

    var tmpSelectArr = data.selectedArr;
    data.selectedArr.map(function(item, index) {
      tempIds.push(item.id);
    });
    data.selectedArr.map(function(item, index) {
      var index2 = common.indexOf(tmpSelectArr, item);
      if (index2 !== -1) {
        tmpSelectArr.splice(index2, 1);
        data.totalPrice = common.total(data.selectedArr).totalPrice;
        data.totalAmount = common.total(data.selectedArr).totalAmount;
      }
    });

    data.selectedArr = [];
    that.setData(data);
    params.good = JSON.stringify(tempIds);
    API.APICart._delete(params).then(d => {
      wx.hideToast();
      var data = this.data;
      if (d.data.goods_groups) {
        data.goods_groups = d.data.goods_groups;
      }

      data.modalHiddenAll = 'true';
      that.setData(data);
      //判断是否删除完了
      if (data.goods_groups[0].goods.length == 0) {
        this.setData({
          show: true,
        })
      }
      this.recomputePrice();
    });

    // API.APICart._clear().then(d => {
    //   wx.hideToast();
    //   //再购物车全部商品中删除选中的
    //   data.selectedArr.map(function (item, index) {
    //     if (common.indexOf(data.items, item) !== -1) {
    //       data.items.splice(common.indexOf(data.items, item), 1);
    //     }
    //   })

    //   //清空选中
    //   data.selectedArr = [];
    //   this.setData(data);

    //   //计算总价
    //   data.totalPrice = common.total(data.selectedArr).totalPrice;
    //   data.totalAmount = common.total(data.selectedArr).totalAmount;
    //   data.type = 'circle';

    //   data.modalHiddenAll = 'true';

    //   //如果删除完了显示空页面
    //   if (data.items.length == 0) {
    //     data.show = true;
    //   }

    //   this.setData(data);
    //   //判断是否删除完了
    //   if (data.items.length == 0) {
    //     this.setData({
    //       show: true,
    //       type: 'circle'

    //     })
    //   }
    // });


  },

  cancelAll: function () {
    this.setData({
      modalHiddenAll: true
    })
  },

  goShopping: function () {
    wx.switchTab({
      url: '../index/index',
    })
  },

  goLogin: function () {
    wx.navigateTo({
      url: '../authorize/index',
    })
  },

})

function accAdd(arg1, arg2) {
  var r1, r2, m;
  try { r1 = arg1.toString().split(".")[1].length } catch (e) { r1 = 0 }
  try { r2 = arg2.toString().split(".")[1].length } catch (e) { r2 = 0 }
  m = Math.pow(10, Math.max(r1, r2))
  return (arg1 * m + arg2 * m) / m
}
