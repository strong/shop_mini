var API = require('../../api/api.endpoint.js');
//获取应用实例
var app = getApp()

Page({

  data: {
    userInfo: {},
    nicknameInput: "",
  },

  //事件处理函数，保存
  saveNickName: function () {
    var that = this;
    var data = this.data;

    var nickname = data.nicknameInput;
    if (nickname.match(/^[ ]+$/) || nickname.length < 3 || nickname.length > 25) {
      app.showTips("请输入3-25个文字作为昵称");
      return;
    }

    var params = {};
    params.nickname = nickname;
    app.showLoading()
    API.APIProfileUpdate.profileUpdate(params).then(d => {
      app.hideLoading();
      that.setData({
        userInfo: d.data.user,
      });
      var pages = getCurrentPages();
      var prevPage = pages[pages.length - 2];
      prevPage.setData({
        showName: d.data.user.nickname,
      });
      app.showTips('修改昵称成功');
      setTimeout(() => {
        wx.navigateBack({
          url: '../myInfo/index'
        })
      }, 1000);
    }).catch(e=>{
      app.hideLoading();
      app.showTips(e);
    });
  },

  //下拉刷新
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh()
  },

  onLoad: function (o) {
    var that = this
    if (o.nickname) {
      that.setData({
        nicknameInput: o.nickname
      })
    }
    // //调用应用实例的方法获取全局数据
    // app.getUserInfo( function( userInfo ) {
    //   //更新数据
    //   that.setData( {
    //     userInfo: userInfo
    //   });
    //   console.log(userInfo);
    // })
  },

  //获取输入的昵称
  nicknameInput: function (e) {
    this.setData({
      nicknameInput: e.detail.value,
    });
  }
})
