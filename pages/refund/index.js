// pages/refund/index.js
var API = require('../../api/api.endpoint.js');
var CONFIG = require('../../config/config.js');
const XXTEA = require('../../utils/xxtea.js');
const Util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: [
      '请选择退款原因',
      '不满意商品'
    ],
    index: 0,
    user: wx.getStorageSync('u'),
    orderModal: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    wx.request({
      url: CONFIG.API_HOST+'/v2/ecapi.pay.refund_reason',
      method: 'post',
      success: function(e){
        console.log(e.data)
        that.setData({
          orderid: options.orderId,
          array: e.data.reason
        })
      }
    })
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  bindPickerChange: function (e) {
    
    this.setData({
      index: e.detail.value
    })
  },//取消订单二次确认
  add: function (e) {

    var index = this.data.index
    if (!index) {
      wx.showModal({
        title: '提示',
        content: '请选择退货原因',
      })
      return;
    }
    this.setData({
      orderModal: false,
    })
  },
  orderYes: function () {
    var orderid = this.data.orderid
    var index = this.data.index
    this.setData({
      orderModal: true,
    })

    wx.showToast({
      title: '请求中',
      icon: 'loading',
      duration: 10000
    });
    var title = this.data.array[index]
    var params = {};
    params.order = orderid;
    params.reason = '1';
    wx.request({
      url: CONFIG.API_HOST + '/v2/ecapi.pay.refund_reason.add',
      method: 'post',
      data: {
        orderid: orderid,
        title: title,
        uid: this.data.user.id
      },
      success: function (e) {
        if (e.data.code == 100) {
          API.APIOrder.cancelOrder(params).then(d => {
            wx.hideToast();



            

            // 刷新列表
            this.getOrderList(true);
          })
          wx.navigateBack({
            delta: 1
          })
        } else {
          wx.showModal({
            title: '提示',
            content: e.data.msg,
          })
        }

      }
    })
    
  },
  orderNo: function () {
    this.setData({
      orderModal: true,
    })
  },
})