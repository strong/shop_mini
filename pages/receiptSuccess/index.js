Page({
  data:{
    orderID : '',
    order:{}
  },

  onLoad: function(param) {
    if (param.order) {
      var order = JSON.parse(param.order);
      if (order) {
        this.setData({
          orderID: order.id,
          order: order
        })
      }
    }
  },

  onShow: function() {
    var data = this.data;
    if (data.isEvaluateResult) {
      this.setData(data);
    }
  },

  //下拉刷新
  onPullDownRefresh: function(){
       wx.stopPullDownRefresh()  
  },

  // 进入订单详情页面
  pushOrderDetailTap: function() {
      wx.navigateTo({
        url : '../orderDetail/index?orderID=' + this.data.orderID
      })
  },

  goEvaluate: function() {
    var orderStr = JSON.stringify(this.data.order); // 将order对象转为json串传递，
    wx.navigateTo({
      url: '../evaluate/index' + '?order=' + orderStr,
    })
  }
})