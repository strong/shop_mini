var API = require('../../api/api.endpoint.js');
var CONFIG = require('../../config/config.js');
var app = getApp();
var common = require('../../utils/util.js');
var RED = require('../common/red.js');
Page({

  data: {
    types: [{
      name: '综合',
      active: true
    }, {
      name: '销量'
    }, {
      name: '价格'
    }, {
      name: '新品'
    }],
    loading: false,
    hasMore: true,
    allowLoadMore: true,
    PER_PAGE: 8,
    sort_key: 0, // 默认
    sort_value: 2, // 降序
    showView: false, //getApp().globalData.showView
    api_host: CONFIG.API_HOST,
    commmon_paket: false,
    price_sort:0,//默认价格排序样式
  },

  // 切换排序方式
  tapSortWay: function(e) {
    var data = this.data;

    data.types.map(function(type, index) {
      type.active = false;
    })

    var index = parseInt(e.currentTarget.id);

    data.types[index].active = true;
    this.setData(data);

    if (index == 0) {
      
      // 综合排序
      data.sort_key = 0; // 默认
      data.sort_value = 2; // 降序
      data.price_sort = 0;

    } else if (index == 1) {

      // 销量排序
      data.sort_key = 4; // 销量
      data.sort_value = 2; // 降序 
      data.price_sort = 0; 

    } else if (index == 2) {

      var price_sort = data.price_sort
      if (price_sort == 0 || price_sort == 2) {

        // 最贵排序
        data.sort_key = 1; // 价格
        data.sort_value = 2; // 降序
        data.price_sort = 1

      } else {

        //最便宜排序
        data.sort_key = 1; // 价格
        data.sort_value = 1; // 升序
        data.price_sort = 2

      }

    } else if (index == 3) {

      // 新品排序
      data.sort_key = 5; // 上架时间
      data.sort_value = 2; // 降序 
      data.price_sort = 0;

    }

    this.reloadProductList(data);
  },

  onLoad: function(option) {
    var data = this.data;
    data.categoryId = option.cid;
    data.loading = true;
    this.reloadProductList(data);
    this.setData(data);
    RED.checkRed(this);
  },

  reloadProductList: function(data) {

    var params = {};
    params.category = data.categoryId;
    params.page = 1;
    params.per_page = data.PER_PAGE;
    params.sort_key = data.sort_key;
    params.sort_value = data.sort_value;

    API.APIProduct.getProductList(params).then(d => {
      var that = this;
      var data = this.data;
      data.products = d.data.products;
      data.loading = false;
      data.hasMore = d.data.paged.more;
      that.setData(data);
    });
  },

  handleLoadMore: function(e) {
    if (!this.data.hasMore) return

    var data = this.data;

    if (data.allowLoadMore) {
      data.allowLoadMore = false;
    } else {
      return;
    }

    data.loading = true;

    var params = {};
    params.category = data.categoryId;
    params.page = data.products.length / data.PER_PAGE + 1;
    params.per_page = data.PER_PAGE;
    data.loading = true;
    this.setData(data);

    // 加载结束后  才能继续加载
    API.APIProduct.getProductList(params).then(d => {
        var that = this;
        var data = this.data;
        data.products = data.products ? data.products.concat(d.data.products) : d.data.products;
        data.loading = false;
        data.hasMore = d.data.paged.more;
        that.setData(data);
        data.allowLoadMore = true;
      })
      .catch(e => {
        data.loading = false;
        this.setData(data);
        console.error(e)
        data.allowLoadMore = true;
      })
  },

  //  tapProduct: function(e) {
  //         var data = this.data;
  //         var product = this.data.products[parseInt(e.currentTarget.id)];
  //         var url = '../productDetail/index?pid='+product.id;
  //         wx.navigateTo({
  //             url: url
  //         });
  tapProduct: function(e) {
    var productId = parseInt(e.currentTarget.id);
    var productIdTuan = parseInt(e.currentTarget.dataset.act_id);
    // var productId = parseInt(product.id);
    // var productIdTuan = parseInt(product.act_id);
    var url = '../productDetail/index?pid=' + productId;
    var urlTuan = '../group/detail?pid=' + productId + "&act_id=" + productIdTuan;
    if (productIdTuan == 0) {
      wx.navigateTo({
        url: url
      });
    } else {
      wx.navigateTo({
        url: urlTuan
      });
    }
  },
  //关闭遮盖
  closeImg: function(e) {
    RED.closeImg(this)
  },
  //显示大的红包
  showView: function() {
    RED.showView(this)
  },
  //领取红包
  getRed: function(e) {
    RED.getRed(this)
  },
  //数据提交事件
  // bindSubmit: function(e) {
  //     console.log('form发生了submit事件，携带数据为：', e.detail.value);
  //     wx.navigateTo({ url: "../productList/index.wxml" });
  // }
})