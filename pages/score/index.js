// pages/score/index.js
var API = require('../../api/api.endpoint.js');
var util = require('../../utils/util.js');
var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    types: [
      { name: '全部', active: true, status: 0 }, { name: '收入', status: 1 }, { name: '支出', status: 2 }
    ],
    currentType: {},
    isEmpty: false,
    historys: [],
    currentScore:0
  },


  tapTabbarItem: function (e) {
    var data = this.data;
    data.types.map(function (type, index) {
      type.active = false;
    })

    data.types[parseInt(e.currentTarget.id)].active = true;

    this.setData({
      currentType: data.types[parseInt(e.currentTarget.id)],
    })
    this.setData(data);

    this.reloadHistoryList();
  },


  reloadData: function() {
    var that = this;
    // 获取当前积分
    API.APIScore.getCurrentScore().then(d => {
      that.setData({
        currentScore: d.data.score
      })
    })

    this.reloadHistoryList();
  },

  reloadHistoryList: function() {
    var that = this;
    // 获取历史积分列表
    var params = {};
    params.page = 1;
    params.per_page = 1000;
    params.status = this.data.currentType ? this.data.currentType.status : 0;
    wx.showLoading({
      title: '加载中...'
    });
    API.APIScore.getHistoryScore(params).then(d => {
      wx.hideLoading();
      wx.stopPullDownRefresh();

      var historys = d.data.history;
      for (var i in historys) {
        var data = historys[i];
        data.date = util.formatTimeExact(data.created_at);
      }

      that.setData({
        historys: historys,
        isEmpty: historys.length <= 0
      })
    }).catch(e => {
      wx.hideLoading();
      wx.stopPullDownRefresh();
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var data = this.data;
    this.setData(data);

    wx.startPullDownRefresh();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.reloadData();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
})