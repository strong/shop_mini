//index.js
//获取应用实例
var API = require('../../api/api.endpoint.js');
var REDAPI = require('../../api/api.endpoint.js');
var CONFIG = require('../../config/config.js');
var RED = require('../common/red.js');
var app = getApp();
Page({
  data: {
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    interval: 3000,
    duration: 1000,
    inputFocus: false,
    inputValue: "",
    title: '加载中...',
    loading: false,
    isTopload:false,
    countDownDay: 0,
    countDownHour: 0,
    countDownMinute: 0,
    countDownSecond: 0,
    currentSwiper: 0,
    //showView: getApp().globalData.showView,
    packetData: [],
    api_host: CONFIG.API_HOST,
    //commmon_paket: false,
  },

  onLoad: function(result) {
    console.log(this.data.showView)
    // console.log('page onLoad');
    // console.log(result)
    var that = this;
    //this.tapWxaSignin();

    RED.checkRed(this);


    var CONFIG = require('../../config/config.js');
    //拼团商品
    wx.request({

      url: CONFIG.API_HOST + "/v2/ecapi.group.index",
      method: 'POST',
      data: {},
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },

      success: function(res) {
        
        that.setData({
          goods: res.data.products
        })
        wx.hideNavigationBarLoading();
      }
    })

    wx.request({

      url: CONFIG.API_HOST + "/wx2/", //请求html
      method: 'POST',
      data: {
        name: 'qinziheng',
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: function(res) {

        that.setData({
          categories: res.data.data.categories
        });
        that.setData({
          fdbanners: res.data.data.banners
        });
        that.setData({
          arr_ad0: res.data.data.arr_ad0
        });
        that.setData({
          arr_ad1: res.data.data.arr_ad1
        });
        that.setData({
          arr_ad2: res.data.data.arr_ad2
        });
        that.setData({
          api_best_goods: res.data.data.api_best_goods
        });
        that.setData({
          api_new_goods: res.data.data.api_new_goods
        });
        that.setData({
          api_promote_goods: res.data.data.api_promote_goods
        });
        if (res.data.data.api_promote_goods[0].gmt_end_time > 0) {
          var totalSecond = res.data.data.api_promote_goods[0].gmt_end_time - Date.parse(new Date()) / 1000;

          var interval = setInterval(function() {
            // 秒数  
            var second = totalSecond;
            // 天数位  
            var day = Math.floor(second / 3600 / 24);
            var dayStr = day.toString();
            if (dayStr.length == 1) dayStr = '0' + dayStr;

            // 小时位  
            var hr = Math.floor((second - day * 3600 * 24) / 3600);
            var hrStr = hr.toString();
            if (hrStr.length == 1) hrStr = '0' + hrStr;

            // 分钟位  
            var min = Math.floor((second - day * 3600 * 24 - hr * 3600) / 60);
            var minStr = min.toString();
            if (minStr.length == 1) minStr = '0' + minStr;

            // 秒位  
            var sec = second - day * 3600 * 24 - hr * 3600 - min * 60;
            var secStr = sec.toString();
            if (secStr.length == 1) secStr = '0' + secStr;

            that.setData({
              ff: Date.parse(new Date()) / 1000,
              countDownDay: dayStr,
              countDownHour: hrStr,
              countDownMinute: minStr,
              countDownSecond: secStr,
            });
            totalSecond--;
            if (totalSecond < 0) {

              clearInterval(interval);
              wx.showToast({
                title: '活动已结束',
              });
              that.setData({
                countDownDay: '00',
                countDownHour: '00',
                countDownMinute: '00',
                countDownSecond: '00',
              });
              // that.requestData()
            }
          }.bind(that), 1000);
        }

        wx.hideNavigationBarLoading();
      }


    })
    wx.showNavigationBarLoading();

  },
  onPageScroll:function(e){
    if(e.scrollTop>wx.getSystemInfoSync().windowHeight){
      this.setData({
        isTopload:true
      })
    }else{
      this.setData({
        isTopload:false
      })
    }
  },
  gotop:function(){
    wx.pageScrollTo({
      scrollTop:0
    })
  },
  setTabBarBadge: function(count) {
    if (!app.isLogin() || count <= 0 || count == undefined) {
      if (wx.removeTabBarBadge) {
        wx.removeTabBarBadge({
          index: 2,
        })
      }
    } else {
      if (wx.setTabBarBadge) {
        wx.setTabBarBadge({
          index: 2,
          text: count > 99 ? '99+' : String(count),
        })
      }
    }
  },
  getCartList: function() {
    var that = this;

    API.APICart._get().then(d => {
      var data = that.data;

      if (d.data.goods_groups.length) {

        var tot = 0;
        for (var i = d.data.goods_groups[0].goods.length - 1; i >= 0; i--) {
          var tempGood = d.data.goods_groups[0].goods[i];
          tempGood.type = 'circle';
          tot += parseInt(d.data.goods_groups[0].goods[i]['amount']);
        }
        d.data.goods_groups[0].total_amount = tot
      }

      data.goods_groups = d.data.goods_groups;

      var total = data.goods_groups.length ? data.goods_groups[0].total_amount : 0; // 单店，goods_groups只有一个，直接取第一个

      that.setTabBarBadge(total);
    });
  },
  tapProduct: function(e) {
    var productId = parseInt(e.currentTarget.id);
    var productIdTuan = parseInt(e.currentTarget.dataset.act_id);
    var url = '../productDetail/index?pid=' + productId;
    var urlTuan = '../group/detail?pid=' + productId + "&act_id=" + productIdTuan;
    if (productIdTuan == 0) {
      wx.navigateTo({
        url: url
      });
    } else {
      wx.navigateTo({
        url: urlTuan
      });
    }
  },
  swiperchange: function(e) {
    //FIXME: 当前页码
    //console.log(e.detail.current)
  },
  swiperchangegroup: function(e) {
    this.setData({
      currentSwiper: e.detail.current
    })
  },
  tapAd: function(e) {
    var url = String(e.currentTarget.id);
    if(url){
      wx.navigateTo({
        url: url
      });
    }
    
  },
  //关闭遮盖
  closeImg: function(e) {
    RED.closeImg(this)
  },
  //显示大的红包
  showView: function() {
    RED.showView(this)
  },
  //领取红包
  getRed: function(e) {
    RED.getRed(this)
  },
  bindInput: function(e) {
    this.setData({
      inputFocus: true,
      inputValue: e.detail.value
    });
  },
  tapCancel: function(e) {
    this.setData({
      inputFocus: false,
      inputValue: ""
    });
  },
  bindChange: function(e) {
    var url = "../searchResult/index?keyword=" + this.data.inputValue;
    wx.navigateTo({
      url: url
    });
  },
  // 页面渲染完成后 调用
  //cell事件处理函数  
  bindCellViewTap: function(e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../babyDetail/babyDetail?id=' + id
    });
  },
  onShareAppMessage: function() {
    // var query = `?pid=${this.data.product.id}&uid=${app.globalData.userInfo.id}`;
    return {
      title: '小麦优选',
      path: 'pages/index/index',
    }
  },
  onShow: function() {
    var that = this
    if (app.isLogin()) {
      this.getCartList(); //获取购物车数量
    }

  }
 

})