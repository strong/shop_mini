//index.js
//获取应用实例
var API = require('../../api/api.endpoint.js');
Page({
    data: {
  
        inputFocus: false,
        inputValue: "",
        title: '加载中...',
        loading: false,
		
 
    },
 
onLoad: function (options) {
	var that = this;
	var CONFIG = require('../../config/config.js');
    wx.request({
	
      url: CONFIG.API_HOST + "/wx2/bills.php",
        method: 'POST',
        data: {
            name: 'qinziheng',
          },
          header: {
          'content-type': 'application/x-www-form-urlencoded'
      },
	
      success: function (res) {
       // console.log(res.data)
        that.setData({ goods: res.data.data.goods }); 
        wx.hideNavigationBarLoading();
      }
    })
 
    //  console.log(app.globalData.openid);
  wx.showNavigationBarLoading();
   
  },

tapMenuItem: function(e) {
 
    this.setData({  
      currentItemId:e.currentTarget.id   
    })  
    },
	
/*tapName: function(event) { 
 console.log(event) ;
  wx.pageScrollTo({
  scrollTop: event-800,
  duration: 300
})
  },*/
  

tapProductList: function(e) {
        var CategoryId = parseInt(e.currentTarget.id);
		var url = '../productList/index?cid='+CategoryId;
        wx.navigateTo({
            url: url
        });
   },
tapProduct: function(e) {
        var productId = parseInt(e.currentTarget.id);
        var url = '../productDetail/index?pid=' + productId;
        wx.navigateTo({
            url: url
        });
    },


  tapAd: function(e) {
    var url = String(e.currentTarget.id);
        wx.navigateTo({
          url: url
        });
    },
    bindInput: function(e) {
        this.setData({
            inputFocus: true,
            inputValue: e.detail.value
        });
    },

    tapCancel: function(e) {
        this.setData({
            inputFocus: false,
            inputValue: ""
        });
    },
    bindChange: function(e) {
        var url = "../searchResult/index?keyword=" + this.data.inputValue;
        wx.navigateTo({ url: url });
    },

})