// pages/changePassword/index.js

var API = require('../../api/api.endpoint.js');
//获取应用实例
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    oldPassword: null,
    newPassword:null,
    affirmNewPassword:null
  },

  changeInput: function(e) {
    var id = e.currentTarget.id;
    if (id == 'oldPassword') {
      this.setData({
        oldPassword: e.detail.value
      })
    } else if (id == 'newPassword') {
      this.setData({
        newPassword: e.detail.value
      })
    } else if (id == 'affirmNewPassword') {
      this.setData({
        affirmNewPassword: e.detail.value
      })
    }
  },

  save: function() {
    var that = this;
    var data = this.data;

    if (!data.oldPassword) {
      app.showTips("请输入旧密码");
      return;
    }
    if (data.oldPassword.length < 6 || data.oldPassword.length > 20) {
      app.showTips('请输入6-20位数字/字母/字符的密码');
      return;
    }
    if (!data.newPassword) {
      app.showTips("请输入新密码");
      return;
    }
    if (data.newPassword.length < 6 || data.newPassword.length > 20) {
      app.showTips('请输入6-20位数字/字母/字符的密码');
      return;
    }
    if (!data.affirmNewPassword) {
      app.showTips("请确认新密码");
      return;
    }
    if (data.newPassword != data.affirmNewPassword) {
      app.showTips("两次密码不一致");
      return;
    }

    var params = {};
    params.old_password = data.oldPassword;
    params.password = data.newPassword;
    app.showLoading()
    API.APIUser.passwordUpdate(params).then(d => {
      app.hideLoading();
      
      app.showTips('修改密码成功');
      setTimeout(() => {
        wx.navigateBack();
      }, 1000);
    }).catch(e => {
      app.hideLoading();
      app.showTips(e);
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },
})