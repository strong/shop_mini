// pages/text/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tickets: app.globalData.tickets,
    opengid: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showShareMenu({
      withShareTicket: true
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var tickets = app.globalData.tickets
    var that = this
    if(tickets){
      wx.getShareInfo({
        shareTicket: tickets,
        success: function (e) {
          var encryData = e.encryptedData;
          var iv = e.iv;
          var sessionkey = wx.getStorageSync('session_key')
          
          wx.request({
            url: 'https://api.xiaomaiyouxuan.com/v2/ecapi.wx.jiemi',
            method:'post',
            data:{
              encryData: encryData,
              iv:iv,
              sessionkey: sessionkey
            },
            dataType:'json',
            success:function(e){
              console.log(e.data,1111)
              if(e.data.code == 100){
                that.setData({
                  opengid: e.data.data.openGId
                })
              }
              
            }
          })
        }
      })
    }
    

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  ondisplaygroupname_error: function(e){
    console.log(e)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '自定义转发标题',
      path: '/pages/text/index',
      
    }
  }
})