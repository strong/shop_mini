var API = require('/api/api.endpoint.js');

//app.js
App({

  onLaunch: function (options) {
    if (wx.canIUse('getUpdateManager')) {
      const updateManager = wx.getUpdateManager()
      updateManager.onCheckForUpdate(function (res) {
        // 请求完新版本信息的回调
        if (res.hasUpdate) {
          updateManager.onUpdateReady(function () {
            updateManager.applyUpdate()
          })
          updateManager.onUpdateFailed(function () {
            // 新的版本下载失败
            wx.showModal({
              title: '已经有新版本了哟~',
              content: '新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~',
            })
          })
        }
      })
    } else {
      // 如果希望用户在最新版本的客户端上体验您的小程序，可以这样子提示
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
    //调用API从本地缓存中获取数据
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
    this.globalData.token = wx.getStorageSync('t', null);
    this.globalData.userInfo = wx.getStorageSync('u', null);
    this.globalData.openid = wx.getStorageSync('o', null);
    //options.query.uid = 17
    if(options.query.uid){
      wx.login({
        success: function (res) {
          var VENDER_WXA = 5;
          var params = {
          };
          params.vendor = VENDER_WXA;
          params.js_code = res.code;
          if (getApp().globalData.sharerUid) { // 如果有推荐人id，需要传推荐人id参数
            params.invite_code = getApp().globalData.sharerUid;
          }

          API.APIAuthSocial.authSocial(params).then(d => {

            var openid = d.data.openid;

            getApp().globalData.openid = openid;
            wx.setStorageSync('o', openid);

            wx.setStorageSync('t', d.data.token);
            getApp().globalData.token = d.data.token;
            // return ;
            var userInfo = d.data.user;
            // var wxUserInfo = that.data.userInfo;
            // userInfo.nickname = wxUserInfo.nickName
            // var photo = {};
            // photo.thumb = wxUserInfo.avatarUrl;
            // photo.large = wxUserInfo.avatarUrl;
            // userInfo.photo = photo;
            // userInfo.gender = wxUserInfo.gender;
            wx.setStorageSync('u', userInfo);
            getApp().globalData.userInfo = userInfo;

            
            app.hideLoading();
            //console.log(d)
           

           

          }).catch(e => {
            app.hideLoading();
            app.showTips(e);
          })
          
        },
        fail: function (res) {
          app.hideLoading();
          app.showTips('微信登录失败');
        }
      });
    }
    
  },

  onPageNotFound: function () {
    // TODO: 当跳转的页面不存在时，会调用此方法
  },

  getUserInfo: function () {
    // console.log('userInfo :' + JSON.stringify(this.globalData.userInfo));
    return this.globalData.userInfo;
  },

  login: function () {
    var that = this;
    //调用登录接口
    var url = '../authorize/index';
    wx.navigateTo({
      url: url
    });
  },

  showTips: function(tips) {
    if(tips){
      wx.showToast({
        title: tips,
        icon: 'none'
      })
    }
    
  },

  showLoading: function() {
    wx.showLoading({
      title: '加载中...',
    })
  },
  hideLoading: function() {
    wx.hideLoading()
  },

  isLogin: function () {
    return this.globalData.token ? true : false;
  },

  onShow: function (options) {
    // console.log(options)
    //if (options.scene == 1007 || options.scene == 1008 || options.scene == 1001) { // 通过分享消息进来的
      if (options.query && options.query.uid) {
        this.globalData.sharerUid = options.query.uid;
      }
    //}
    console.log(options)
    this.globalData.tickets = options.shareTicket
  },

  onHide: function () {
    console.log('App Hide')
  },

  cartTotal: function () {
    var count = 0;
    if (this.globalData.cartData) {
      if (this.globalData.cartData.goods) {
        var goods = this.globalData.cartData.goods;
        for (var j = 0; j < goods.length; ++j) {
          count += goods[j].amount;
        }
        this.globalData.cartData.cartTotal = count;
        return count;
      }

    }
    return 0;
  },

  setTabBarBadge: function (count) {
    if (!this.isLogin() || count <= 0 || count == undefined) {
      if (wx.removeTabBarBadge) {
        wx.removeTabBarBadge({
          index: 2,
        })
      }
    } else {
      if (wx.setTabBarBadge) {
        wx.setTabBarBadge({
          index: 2,
          text: count > 99 ? '99+' : String(count),
        })
      }
    }
  },

  updateCartCount:function() {
    if (!this.isLogin()) {
      return;
    }
    API.APICart._get().then(d => {
      var total = d.data.goods_groups.length ? d.data.goods_groups[0].total_amount : 0; // 单店，goods_groups只有一个，直接取第一个
      this.globalData.cartData = { 'cartTotal': total };
      this.setTabBarBadge(total);
    })
  },

  globalData: {
    userInfo: null,
    openid: null,
    token: null,
    confirmOrderData: {},
    confirmProductData: {},
    cartData: { 'cartTotal': 0 },
    config: {},
    sharerUid: null, // 分享者的uid, 如果是通过别人的分享打开的小程序时有值
    showView: false,
    commmon_paket: false,
    tickets: ''
  },
})
