//时间转换
function formatTime(stamp) {
    var date = new Date(stamp*1000)
    var year = date.getFullYear()
    var month = date.getMonth() + 1
    var day = date.getDate()

    // var hour = date.getHours()
    // var minute = date.getMinutes()
    // var second = date.getSeconds()


    return [year, month, day].map(formatNumber).join('.');
    //+ ' ' + [hour, minute, second].map(formatNumber).join(':')
}

//时间转换
function formatTimeLine(stamp) {
  var date = new Date(stamp * 1000)
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  // var hour = date.getHours()
  // var minute = date.getMinutes()
  // var second = date.getSeconds()


  return [year, month, day].map(formatNumber).join('-');
  //+ ' ' + [hour, minute, second].map(formatNumber).join(':')
}

/**
 * 时间格式化，精确的时分秒 格式例：2016-5-16 20:09:30
*/
function formatTimeExact(stamp) {
  var date = new Date(stamp * 1000)
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()


  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}


function formatNumber(n) {
    n = n.toString()
    return n[1] ? n : '0' + n
}
// 匿名处理
function formatNameAnony(n) {
  n = n.toString()
  var length = n.length;
  var start = n.substr(0, 1);
  var end = n.substr(length-2,2);
  return start + "***" + end;
}

function indexOf(arr,el) {
    for (var i = 0, n = arr.length; i < n; i++) {
        if (arr[i].id === el.id) {
            return i;
        }
    }
    return -1;
}

//购物车选择时计算总价
function total(arr){
    var amount = 0;
    var price = 0;
    arr.map(function(item,index){
        amount += item.amount;
        price += item.amount*item.price;
    })
    return{totalAmount: amount,totalPrice: price};
}

/*
  判断字符串是否为纯数字
*/
function isOnlyNumbers(s) {
  var regNum = new RegExp('[^0-9]', 'g');
  return !regNum.test(s);
}
/*
  判断字符串是否为纯小写字母
*/
function isOnlyLowercaseLetters(s) {
  var regLowerCase = new RegExp('[^a-z]', 'g');
  return !regLowerCase.test(s);
}
/*
  判断字符串是否为纯大写字母
*/
function isOnlyCapitalLetters(s) {
  var regCapitalLetter = new RegExp('[^A-Z]', 'g');
  return !regCapitalLetter.test(s);
}
/*
  判断字符串是否为纯字母, 不区分大小写
*/
function isOnlyLetters(s) {
  var regLetter = new RegExp('[^A-z]', 'g');
  return !regLetter.test(s);
}
/*
  判断字符串是否包含中文
*/
function isContainChinese(s) {
  var regChinese = new RegExp('[\u4e00-\u9fa5]', 'g');
  return regChinese.test(s);
}
/*
  去掉字符串首尾两边空格
*/
function trim(s) {
  return s.replace(/(^\s*)|(\s*$)/g, "");
}
/*
  去掉字符串左边空格
*/
function trimL(s) {
  return s.replace(/(^\s*)/g, "");
}
/*
  去掉字符串右边空格
*/
function trimR(s) {
  return s.replace(/(\s*$)/g, "");
}

module.exports = {
    formatTime: formatTime,
    formatTimeExact: formatTimeExact,
    indexOf: indexOf,
    total: total,
    formatNameAnony: formatNameAnony,
    formatTimeLine: formatTimeLine,
    isOnlyNumbers: isOnlyNumbers,
    isOnlyLowercaseLetters: isOnlyLowercaseLetters,
    isOnlyCapitalLetters: isOnlyCapitalLetters,
    isOnlyLetters: isOnlyLetters,
    isContainChinese: isContainChinese,
    trim: trim,
    trimL: trimL,
    trimR: trimR,
}
