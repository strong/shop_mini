const CONFIG = require('../config/config.js');

const fetch = require('./fetch');

/**
 * 获取列表类型的数据
 * @return {Promise}       包含抓取任务的Promise
 */

function getArticleList (params) {
    return fetch(CONFIG.API_HOST,'v2/ecapi.article.list', params);
}

function getAdList(params){
  return fetch(CONFIG.API_HOST, 'v2/ecapi.ad.get_ads', params);
}

module.exports = { getArticleList, getAdList };

