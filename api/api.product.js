var CONFIG = require('../config/config.js');

const fetch = require('./fetch');

/**
 *  获取商品分类列表
 * @return {Promise}       包含抓取任务的Promise
 */

function getProductList (params) {
    return fetch(CONFIG.API_HOST,'/v2/ecapi.product.list', params);
}

function getProduct (params) {
    return fetch(CONFIG.API_HOST,'/v2/ecapi.product.get', params);
}

function purchase (params) {
  return fetch(CONFIG.API_HOST,'/v2/ecapi.product.purchase', params);
}

function likeProduct(params) {
  return fetch(CONFIG.API_HOST, '/v2/ecapi.product.like', params);
}

function unlikeProduct(params) {
  return fetch(CONFIG.API_HOST, '/v2/ecapi.product.unlike', params);
}

function getReviews(params) {
  return fetch(CONFIG.API_HOST, '/v2/ecapi.review.product.list', params);
}

function getAccessoryProduct(params) {
  return fetch(CONFIG.API_HOST, '/v2/ecapi.product.accessory.list', params);
}

function getRecommentProduct(params) {
  return fetch(CONFIG.API_HOST, '/v2/ecapi.recommend.product.list', params);
}
function browseNumber(params) {
  return fetch(CONFIG.API_HOST, '/v2/ecapi.goods.addclick', params);
}


module.exports = { getProductList,
					getProduct,
					purchase ,
          likeProduct,
          unlikeProduct,
          getReviews,
          browseNumber,
          getAccessoryProduct,
          getRecommentProduct};
