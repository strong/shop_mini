var CONFIG = require('../config/config.js');

const fetch = require('./fetch');
// 取消订单
function group_detail(params) {
  return fetch(CONFIG.API_HOST, 'v2/ecapi.group.detail', params);
}

function group_order(params) {
  return fetch(CONFIG.API_HOST, 'v2/ecapi.group.groupmember', params);
}

module.exports = {
  group_detail,
  group_order
};