var CONFIG = require('../config/config.js');

const fetch = require('./fetch');

/**
 *  获取收藏列表
 * @return {Promise}       包含抓取任务的Promise
 */

function getFavoriteList(params) {
  return fetch(CONFIG.API_HOST, '/v2/ecapi.product.liked.list', params);
}

module.exports = {
  getFavoriteList
};