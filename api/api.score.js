
var CONFIG = require('../config/config.js');

const fetch = require('./fetch');

/**
 * 获取列表类型的数据
 * @return {Promise}       包含抓取任务的Promise
 */

function getCurrentScore(params) {
  return fetch(CONFIG.API_HOST, 'v2/ecapi.score.get', params);
}

function getHistoryScore(params) {
  return fetch(CONFIG.API_HOST, 'v2/ecapi.score.history.list', params);
}

module.exports = { getCurrentScore, getHistoryScore };