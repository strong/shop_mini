var CONFIG = require('../config/config.js');

const fetch = require('./fetch');

/**
 *  获取红包
 * @return {Promise}       包含抓取任务的Promise
 */

function getRedPacket(params) {
  return fetch(CONFIG.API_HOST, '/v2/ecapi.cashgift.getone', params);
}
function bindRedPacket(params) {
  return fetch(CONFIG.API_HOST, '/v2/ecapi.cashgift.bindgift', params);
}

function isReceiveRedBonus(params) {
  return fetch(CONFIG.API_HOST, '/v2/ecapi.cashgift.isreceiveredbonus', params);
}

module.exports = {
  getRedPacket,
  bindRedPacket,
  isReceiveRedBonus
};