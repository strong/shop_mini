const CONFIG = require('./../config/config.js');

const fetch = require('./fetch');

/**
 * 获取列表类型的数据
 * @return {Promise}       包含抓取任务的Promise
 */

function signin (params) {
    return fetch(CONFIG.API_HOST,'v2/ecapi.auth.signin', params);
}

/**
 * 获得手机验证码
*/
function sendPhoneVerifyCode (params) {
  return fetch(CONFIG.API_HOST, 'v2/ecapi.auth.mobile.send', params);
}
/**
 * 验证手机号
*/
function verifyPhoneNumber(params) {
  return fetch(CONFIG.API_HOST, 'v2/ecapi.auth.mobile.verify', params);
}

/**
 * 手机号注册
*/
function signupByPhone (params) {
  return fetch(CONFIG.API_HOST, 'v2/ecapi.auth.mobile.signup', params);
}

/**
 * 手机号重置密码
*/
function forgotByPhone(params) {
  return fetch(CONFIG.API_HOST, 'v2/ecapi.auth.mobile.reset', params);
}

/**
 * 邮箱注册
*/
function signupByEmail (params) {
  return fetch(CONFIG.API_HOST, 'v2/ecapi.auth.default.signup', params);
}

/**
 * 邮箱重置密码
*/
function forgotByEmail (params) {
  return fetch(CONFIG.API_HOST, 'v2/ecapi.auth.default.reset', params);
}

module.exports = {
  signin,
  sendPhoneVerifyCode,
  verifyPhoneNumber,
  signupByPhone,
  forgotByPhone,
  signupByEmail,
  forgotByEmail
};
